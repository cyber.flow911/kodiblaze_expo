import React, { Component,useState,useEffect } from 'react'
import { Text, StyleSheet, View, Image, ImageBackground, ScrollView,TouchableWithoutFeedback,Dimensions,TouchableOpacity,FlatList } from 'react-native'
import {assets, theme,songBaseUrl} from '../config'
import { Ionicons } from '@expo/vector-icons';
import CardView from '../Utils/CardView';
import { Feather, Octicons,FontAwesome5 } from '@expo/vector-icons'
import { Avatar, Badge, Icon, withBadge } from 'react-native-elements'
import { Audio, Video } from 'expo-av';
import {fetchAllSongs,checkSong} from '../../Apis/Songs'
import PaymentModal from '../PaymentModal'
import {useDispatch,useSelector} from 'react-redux';
import SuccessModal from '../Utils/SuccessModal'
import {fetchUserSongs} from '../../Apis/Profile'
import Shimmer from '../Utils/Shimmer'

const width = Dimensions.get('window').width
const height = Dimensions.get('screen').height

export default function Songs(props) {

   
    const [isPlaying , setIsPlaying] = useState(false);
    const [playingSongId , setPlayingSongId] = useState(null);
    const [songName , setSongName] = useState('');
    const [sound, setSound] = useState();
    const [globalSongUrl , setSongUrl] = useState('');
    const [paymentModal, setPaymentModal] = useState(false);
    const [songDetails, setSongDetails] = useState(null);
    const userDetails = useSelector((state)=>state.user.userDetails);
    const [paymentComplete, setPaymentComplete] = useState(false);
    const [successModal, setSuccessModal] = useState(false);
    const [userSongs , setUserSongs] = useState([])
    const [songIndex , setIndex] = useState();
    const [songs , setSongs] = useState(
    [ ]
  );
    const [pageLoader , setPageLoader] = useState(false);

        //   function to pause song on navigation change
    useEffect( () => {
        const unsubscribe = props.navigation.addListener('blur', async() => {
          await sound.playAsync();
          setIsPlaying(false);
        });
        return unsubscribe; // will be called when the component unmount occurs
      }, [props.navigation]);
    

    useEffect(() => {
        fetchAllSongs(songsCallback);
        fetchUserSongs(userDetails.id,userSongsCallback);
        setPageLoader(true);
    },[]);

    useEffect(() => {
        if(songs && userSongs)
        {
            var oldSongs = [...songs];
            for(var i=0;i<oldSongs.length;i++)
            {
                var songId = oldSongs[i].id;
                for(var j=0;j<userSongs.length;j++)
                {
                    if(userSongs[j].songId == songId){
                        oldSongs[i].paid = true;
                    }
                }
            }
            // console.log("oldSongs",oldSongs);
            setSongs(oldSongs);
        };
    },[userSongs])

    function userSongsCallback(response){
        // console.log(response)
        if(response.msg == 'success')
            setUserSongs(response.data);
    }

    function songsCallback(response) {
        setSongs(response.songs);
        setPageLoader(false)
    }

  async function playThisSong(song) {
    const songUrl = songBaseUrl + song.song
    setSongUrl(songUrl)
    const { sound } = await Audio.Sound.createAsync(
            {uri:songUrl}
        );
    setSongName(song.name);     
    setPlayingSongId(song.id);
    setIsPlaying(true);
    setSongDetails(song)
    setSound(sound);

    await sound.playAsync(); 
  }

  async function pauseSound() {
    const data = await sound.getStatusAsync()
    setIsPlaying(false);
    await sound.pauseAsync(); 
}

 async function playSound() {
    const { sound } = await Audio.Sound.createAsync(
        {uri:globalSongUrl}
    );
    // setSongName(song.name);     
    // setPlayingSongId(song.id);
    setIsPlaying(true);
    setSound(sound);

    await sound.playAsync(); 
 }
useEffect(() => {
    return sound
    ? () => {
        sound.unloadAsync(); }
    : undefined;
}, [sound]);


    useEffect(() => {
        if(paymentComplete)
        {    
            setSuccessModal(true);
            var oldSongs = [...songs];
            oldSongs[songIndex].downloads++;
            oldSongs[songIndex].paid = true;
            setSongs(oldSongs);
            var selectedSongDetails = songDetails;
            selectedSongDetails.paid = true;
            setSongDetails(selectedSongDetails);
            setPaymentComplete(false);
        }
    },[paymentComplete]);

  const renderSongsList=(item,index)=>{
      setIndex(index);
    return(
        <TouchableWithoutFeedback onPress={()=>playThisSong(item)}>
                <View style={{display: 'flex', flex: 1,flexDirection: 'row',margin:10}}>
                <Image source={assets.profile.songImage} style={{height:60,width:60}} />
                <View style={[styles.contestBodyWrapper,{marginLeft:10}]}>
                    <Text style={[styles.contetsHeading,{fontSize:18}]}>{item.name}</Text>
                    <Text style={[styles.contestBody,{fontSize:12}]}>{item.downloads} Downloads  {item.paid?(null):('$'+item.price)}</Text>
                </View> 
                {item.paid?(null):(
                    <View style={[styles.contestBodyWrapper,{marginLeft:10}]}>
                        <Text style={[styles.contetsHeading,{fontSize:18,position:'absolute',left:120}]}>
                            <FontAwesome5 name="dollar-sign" size={24} color="white" />    
                        </Text>
                    </View> 
                )}        
            </View>
        </TouchableWithoutFeedback>
    )
}

function handleBuy()
{
    if(songDetails)
        {setPaymentModal(true);
        pauseSound()}
    else    
        alert("Select Song First!");
}
        return (
            <View styles={styles.container}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
            <PaymentModal modalVisible={paymentModal} setModalVisible={setPaymentModal} song={songDetails} userDetails={userDetails} setPaymentComplete={setPaymentComplete}/>
            <SuccessModal showModal={successModal} setModalVisible={setSuccessModal} displayTitle={'Payment Successful'} navigateToHome={()=>setSuccessModal(false)} />
                {/* <ScrollView> */}
                    {/* {renderVideoCard()} */}
                        {CardView(
                        <View>
                            <View style={{display: 'flex',flexDirection: 'row',margin:10}}>
                                <Image source={assets.profile.songImage} style={{height:60,width:60}} />
                                <View style={[styles.contestBodyWrapper,{marginLeft:10}]}>
                                 <Text style={[styles.contetsHeading,{fontSize:14,marginTop:10}]}>{songName}</Text>
                                 </View>
                                <TouchableOpacity style={{position: 'absolute',right:10,top:15}} 
                                onPress={() => {isPlaying?(pauseSound()):(playSound())}} >
                                    <Ionicons name={isPlaying? 'pause-outline' : 'play-outline'} size={30} color="white" />
                                </TouchableOpacity>             
                            </View>
                        </View>,
                        {width: width/1.8, height: height/10, marginLeft: '5%', marginRight:'auto', borderRadius: 50,position:'absolute',top:height/20,opacity:0.96},10,'#303030'
                    )}
                    <View >
                        <TouchableOpacity  style={{position: 'absolute',right:50,top:60}} onPress={()=>{handleBuy()
                        }}>
                               {songDetails?(
                                   songDetails.paid?(null):(
                                    <>
                                    <Ionicons name={'card'} size={40} color="white" /> 
                                    <Text style={{color: 'white',fontSize:18}}>Buy</Text>
                                    <Badge status="error" value="1" containerStyle={{position: 'absolute',right:-6,top:-3}}/>
                                    </>
                                   )
                               ):(
                                    null
                               )}
                                   
                        </TouchableOpacity>  
                        
                    </View>
                {CardView(
                        <View style={styles.container}>
                            <View styles={[styles.header,{marginTop:10}]}>
                                <Text style={[styles.contetsHeading,{fontSize:18,margin:20}]}>All Songs</Text>

                            </View>
                            {
                                pageLoader?(
                                    <View style={{marginLeft:20}}>
                                        <Shimmer width={width-60} height={80} />
                                    </View>
                                ):
                                (songs?(
                                <FlatList
                                    data={songs}
                                    renderItem={({item,index})=>renderSongsList(item,index)}
                                    keyExtractor={(item)=>item.id}
                                    horizontal={false}
                                    
                                /> ):(
                                    <Text style={{color:'white',fontSize:15,marginLeft:20}}>No Songs</Text>
                            ))}          
                        </View>,
                        {width: width, height: height/1.5, marginLeft: 'auto', marginRight:'auto', borderRadius: 30,position:'absolute',top:height/5,opacity:0.96},10,'#303030'
                    )}
                
            
            </ImageBackground>
            </View>
        )
    }

const styles = StyleSheet.create({
    bgImage:{
        height:'100%',
        width:'100%',
    },
    container:{
        paddingHorizontal: 10,
        flex: 1,
    },
    HeaderButtonWrapper:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal:30,
        marginTop:50,
        marginBottom:30,
    },
    headerBtn:{
        width:80.1,
        height:24
    },
    headerBtnRecomended:{
        height:24,
        width:95.8,
    },
    videoCardWrapper:{
        marginBottom:40,
        paddingHorizontal:20,
    },
    VideoPoster:{
        height:130,
        width:'100%',
        borderRadius:18,
    },
    videoTitle:{
        color: theme.whiteColor,
        fontSize:18,
        fontWeight: 'bold',
    },
    videoDescription:{
        color: theme.whiteColor
    },
    VideoContentWrapper:{
        marginTop:6,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    header:
    {
        flexDirection: 'row',
        width: width,
        height: height*0.05,
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginLeft:10,
        padding:10
    },
        postQueText:
        {
           fontSize: 24,
           marginLeft: '5%',
           marginTop: '2%',
           flexDirection: 'row',
           justifyContent: 'center'
        },
    
    queDescView:
    {
        marginTop: -height*0.03,
        padding: 10,
        flexDirection:'row',
        justifyContent: 'center',
    },
        queDesc:
        {
            borderRadius: 10,
            padding: 4,
            // borderWidth: 0.5,
            backgroundColor:'white'
        },
    contestBodyWrapper:{
        // width:170,
        // paddingHorizontal:16,
    },
    contetsHeading:{
        marginTop:4,
        color:'white',
        fontSize:18,
    },
    contestBody:{
        marginTop:4,
        color:'red',
        fontSize:10,
    },
})
