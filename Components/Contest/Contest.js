import React, { useState,useEffect } from 'react'
import { Text, StyleSheet, View, Image, ImageBackground,TouchableOpacity,FlatList, Dimensions } from 'react-native'
import {assets, theme} from '../config'
import { Ionicons } from '@expo/vector-icons';
import {fetchAllContests} from '../../Apis/Contest';
import Shimmer from '../Utils/Shimmer'

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width

export default function Contest(props){

    const [contest , setContests] = useState([]);
    const [filter,setfilter] = useState('ongoing');
    const [loader,setLoader] = useState(true)
    useEffect(() => {
        if(filter != '')
        {
            fetchAllContests(filter,contestCallback);
            setLoader(true);
        }    
    },[filter]);

    function contestCallback(response){
        if(response.msg == 'success')
        {
            setContests(response.contests);
            setLoader(false)
        }
    }

    const renderContestCard=(item)=>{   
        return(
            <TouchableOpacity onPress={() =>{props.navigation.navigate('ContestPage',{contestId: item.id})}}>
                <ImageBackground source={assets.contest.WallPrimaryWrapper} style={styles.contestGradientWrapper}>
                    <View style={styles.GradientInnerWrapper}>
                        <ImageBackground source={assets.contest.WallCard} style={styles.contestWall}>
                                <View>
                                    <Image source={{uri:item.header_image}} style={styles.contestImg} />
                                </View>
                                <View style={styles.contestBodyWrapper}>
                                    <Text style={styles.contetsHeading}>{item.name}</Text>
                                    <Text style={styles.contestBody}>{item.description}</Text>
                                </View>
                        </ImageBackground>
                        <View style={{marginLeft:24,}}>
                            {/* <View>
                                <Ionicons name="heart-outline" size={24} color="white" />
                            </View> */}
                            <View style={{marginTop:14,}}>
                                <Ionicons name="arrow-forward" size={24} color="white" />
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        )
    }


        return (
            <View styles={styles.container}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
            

                <View style={styles.HeaderButtonWrapper}>
                    <TouchableOpacity onPress={()=>{setfilter('ongoing')}}>
                        <Image source={assets.contest.btnOngoing} style={styles.headerBtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{setfilter('upcoming')}}>
                        <Image source={assets.contest.btnUpcoming} style={styles.headerBtn} />
                    </TouchableOpacity>
                </View>
                {loader?(
                    <View style={{marginLeft:20}}>
                        <Shimmer width={width-30} height={120} />
                    </View>
                ):(
                    <FlatList
                        data={contest}
                        renderItem={({item})=>renderContestCard(item)}
                        keyExtractor={(item)=>item.id}  
                        style={{marginLeft:20}}              
                    />
                )}
               
            
            </ImageBackground>
            </View>
        )
}

const styles = StyleSheet.create({
    bgImage:{
        height:'100%',
        width:'100%',
    },
    container:{
        paddingHorizontal: 10,
        flex: 1,
    },
    HeaderButtonWrapper:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal:30,
        marginTop:50,
        marginBottom:30,
    },
    headerBtn:{
        width:118,
        height:40
    },
    contestGradientWrapper:{
        marginBottom:30,
        marginLeft:10,
        height:116.6,
        width:328.33,
    },
    GradientInnerWrapper:{
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
    },
    contestWall:{
        width:250,
        height:110,
        flexDirection: 'row',
        // justifyContent: 'space-between',
    },
    contestImg:{
        marginTop:10,
        marginLeft:8,
        width:80,
        height:90,
        borderRadius:10,
    },
    contestBodyWrapper:{
        width:170,
        paddingHorizontal:16,
    },
    contetsHeading:{
        marginTop:4,
        color:theme.whiteColor,
        fontSize:20,
        textAlign: 'center',
    },
    contestBody:{
        marginTop:4,
        color:theme.whiteColor,
        fontSize:10,
        textAlign: 'center',
    }
})
