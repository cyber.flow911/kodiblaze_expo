import React,{useState, useEffect} from 'react'
import { Dimensions, FlatList, Image, Modal, StyleSheet,TouchableWithoutFeedback,ImageBackground,ActivityIndicator, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { assets, theme,imageBaseUrl,videoBaseUrl } from '../config';
import { Ionicons,FontAwesome5 } from '@expo/vector-icons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import CardView from '../Utils/CardView';
import {fetchSingleContest,voteContestantForContest} from '../../Apis/Contest'
import {useDispatch , useSelector} from 'react-redux'
import {Video} from 'expo-av'
import {SET_VIDEO} from '../../Reducers/types'
import SuccessModal from '../Utils/SuccessModal'
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width


export default function ContestMainPage(props) {

    const [modalVisible, setModalVisible] = useState(false) 
    const contestId = props.route.params.contestId;
    const [contestDetails, setContestDetails] = useState(false);
    const [contestants, setContestants] = useState(false);
    const dispatch = useDispatch();
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]; 
    const [votingContestant, setvotingContestant] = useState(false);
    const [email,setEmail] = useState('');
    const [loader,setLoader] = useState(false);
    const [successModal,setSuccessModal] = useState(false);
    const [index,setIndex] = useState(false);
    const [pageLoader,setPageLoader] = useState(true);


    useEffect(() => {
        fetchSingleContest(contestId,contestCallback);
        setvotingContestant(false);
        setLoader(false);
        setContestDetails(false);
        setContestants(false);
        setPageLoader(true);
    },[contestId])

    function contestCallback(response) {
        if(response.msg == "success"){
            setContestDetails(response.contest);
            if(response.contestants == 'notFound') {
                setContestants(false);
            }
            else
                setContestants(response.contestants);

            setPageLoader(false);
        }
    }

    function voteContestant(){
        if(email == '')
            alert('Please enter address');
        else
        {
            voteContestantForContest(email,contestId,votingContestant.id,votingCallback)
            setLoader(true);
        }
    }

    function votingCallback(response){
        setLoader(false);
        if(response.msg == 'success')
        {
            setModalVisible(false);
            setTimeout(()=>{
                setSuccessModal(true)
            },1000)
            setEmail('');
            const oldContestants = [...contestants];
            //updating Boards status in state array
            oldContestants[index].votes++;
            setContestants(oldContestants);

        }
        else
        {
            alert("You have already voted!");
        }
    }

   
    function setVotingDetails(item) {
        setModalVisible(true);
        setvotingContestant(item)
    }


    const renderContestantData=(item,index)=>{
        return(
            <View style={styles.CardWrapper}>
               <View >
                    <View>
                        {/* <Image source={item.image.includes('https')?({uri:item.image}):({uri:imageBaseUrl+item.image})} style={styles.poster}/> */}
                        <TouchableOpacity onPress={()=>{playVideo(item)}}>
                        <Video
                                style={styles.poster}
                                source={{
                                    uri: (videoBaseUrl+item.video),
                                }}
                                useNativeControls={false}
                                resizeMode="cover"
                                
                                
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between', paddingVertical:4, paddingHorizontal:8,borderRadius:10}}>
                        <View style={{flexDirection: 'column',justifyContent: 'flex-start',flex:0.9}}>
                            <Text style={styles.BlogTitle}>{item.name}</Text>
                            <Text >Vote : {item.votes}</Text>
                        </View>
                        <TouchableOpacity activeOpacity={0.6} style={styles.voteButton} onPress={() => {setVotingDetails(item);setIndex(index)}}>
                            <Text style={{color:'white'}}>Vote</Text>
                        </TouchableOpacity>
                    </View>
               </View>
            </View>
        )
    }

    function generateDate(date) {
        var timeToDate = new Date(date);
        var returedDate = timeToDate.getDate() + ' ' + months[timeToDate.getMonth()] + ' ' + (-100+timeToDate.getYear())  ; 
        return returedDate
    }

    function playVideo(item)
    {
        setModalVisible(false)
        dispatch({type:SET_VIDEO,payload:{recordVideo:true}})
        props.navigation.navigate('PlayRecordedVideo', { videoUrl:videoBaseUrl+item.video,recordedVideo:true,navigate:'ContestPage',contestId:contestId })
    }

    function showWinner(){
        props.navigation.navigate('Winner', { contestId : contestId });
    }

    

    return (
        <>
        {pageLoader?(
            <View style={[styles.container,{marginTop:-50}]}>
                <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
                    <ActivityIndicator size="large"  color="white" style={{marginTop:height/2.2}} />
                </ImageBackground>
            </View>
        ):(
            <View style={[styles.container,{marginTop:-50}]}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
            <TouchableOpacity onPress={()=>{props.navigation.navigate('Contest')}} style={{position: 'relative',top:60,left:10,zIndex:10,marginBottom:20}}>
                    <FontAwesome5 name="arrow-left" size={20} color={'white'} style={{padding:2}} />
                </TouchableOpacity>
                <View style={styles.MainWrapper}>
                    <Image source={contestDetails?(contestDetails.header_image.includes('https')||contestDetails.header_image.includes('http')?({uri:contestDetails.header_image}):({uri:imageBaseUrl+contestDetails.header_image})):(null)} style={styles.VideoPoster} />
                </View>
            {/* <Image source={{uri:'https://picsum.photos/500/500'}} style={styles.headerImg} /> */}
            <View style={{marginTop:14}}>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <Text style={{fontSize:18,color:'white'}}>{contestDetails.name}</Text>
                    {contestDetails.type=='ended'?(
                        <TouchableOpacity onPress={()=>{showWinner()}}>
                            <Image source={assets.video.colorBorder} style={styles.headerBtn} />
                            <View style={{backgroundColor:'black'}}>
                                <Text style={[{fontSize:12,zIndex:10,position: 'relative',bottom:20,left:17,color:'white'}]}>Winner</Text>
                            </View>
                        </TouchableOpacity>
                    ):(
                        null
                    )}                  
                </View>
                <Text style={{color:'white'}}>
                    {contestDetails.description}
                </Text>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop:10}}>
                    <Text style={{color:'white'}}><MaterialCommunityIcons name="calendar" size={24} color="white" /> {generateDate(contestDetails.start_date)}</Text>
                    <MaterialCommunityIcons name="minus" size={24} color="white" />
                    <Text style={{color:'white'}}><MaterialCommunityIcons name="calendar" size={24} color="white" />{generateDate(contestDetails.end_date)}</Text>
                </View>
                <View style={styles.hr}></View>

                {CardView(
                        <View style={[styles.container,{marginLeft:10,marginRight:10}]}>
                            <View styles={[styles.header,{marginTop:10}]}>
                                <Text style={[styles.contetsHeading,{fontSize:18,textAlign: 'start',margin:20}]}>Contestants</Text>
                            </View>
                            {contestants?(
                                    <FlatList
                                    showsVerticalScrollIndicator={false}
                                    data={contestants}
                                    renderItem={({item, index})=>renderContestantData(item, index)}
                                    keyExtractor={(item)=>item.id}
                                    numColumns={2}
                                    
                                    
                                    /> 
                            ):(
                                <Text style={{color:'white',marginLeft:30}}>No Contestant</Text>
                            )}
                                   
                        </View>,
                        {width: width, height: height/1.5, marginLeft: 'auto', marginRight:'auto', borderRadius: 30,position:'absolute',top:height/6,opacity:0.96},10,'#303030'
                    )}

            </View>
            </ImageBackground>
        </View>
        )}
                <SuccessModal showModal={successModal} setModalVisible={setSuccessModal} displayTitle={'You have voted Successfully'} navigateToHome={()=>setSuccessModal(false)} />

        <Modal

                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                // Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
                }}
            >
                    <TouchableWithoutFeedback onPress={()=> {}} >
                            <View style={{width:'100%',height:'100%'}}>
                                <View>
                                    {CardView(
                                    <TouchableWithoutFeedback>
                                        <View style={styles.container}>
                                            <View style={styles.header}>
                                                <TouchableOpacity onPress={()=>{setModalVisible(false)}} style={{padding:0}}>
                                                    <FontAwesome5 name="arrow-left" size={20} color={'black'} style={{padding:2}} />
                                                </TouchableOpacity>
                                                <Image />
                                            </View>
                                            <View
                                             style={{marginLeft:width/17,marginRight:width/17}}
                                             >
                                            <TouchableOpacity onPress={()=>{playVideo(votingContestant)}}>
                                                <Video
                                                        style={styles.VideoPoster}
                                                        source={{
                                                            uri: votingContestant?(videoBaseUrl+votingContestant.video):(null),
                                                        }}
                                                        useNativeControls={false}
                                                        resizeMode="cover"
                                                        
                                                        
                                                    />
                                                </TouchableOpacity>
                                                <View style={styles.InputWrapper}>
                                                    <TextInput placeholder="Enter Email" value={email} onChangeText={(text)=>{setEmail(text)}} placeholderTextColor='white' style={styles.input}/>
                                                </View>
                                                <View style={{ flexDirection: 'row' ,justifyContent:'flex-end'}}>
                                                    {loader?(
                                                        <TouchableOpacity  style={styles.addButton}>
                                                        <ActivityIndicator size="large"  color="white" style={{marginTop:25}} />
                                                        
                                                    </TouchableOpacity>
                                                    ):(<TouchableOpacity onPress={()=>{voteContestant()}} style={styles.addButton}>
                                                        <Text style={{color:'white',fontSize:20}}>
                                                            Vote &nbsp;
                                                            <FontAwesome5 name="arrow-right" size={20} color={'white'} style={{padding:2}} />    
                                                        </Text>
                                                        
                                                    </TouchableOpacity>)}
                                                </View>
                                            {/* <FlatList
                                                data={contest} 
                                                renderItem={({item})=>renderContestCard(item)} 
                                                style={{height:height/2,width:'auto'}}
                                            /> */}
                                            </View>            
                                        </View>
                                        </TouchableWithoutFeedback>,
                                        {width: width, height: height/1.75, marginLeft: 'auto', marginRight:'auto', borderRadius: 20,position:'absolute',top:height/2},10,'white'
                                    )}
                                </View>
                            </View>
                    
                    </TouchableWithoutFeedback>
            </Modal>

{/* 
            <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                    // Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                    }}
                >
                   

                    <View style={styles.modalViewHai}>
                        <View style={{alignItems: 'flex-end', marginBottom:14}}>
                            <MaterialCommunityIcons name="close" size={24} color="black" onPress={() => setModalVisible(!modalVisible)} />
                        </View>
                                                
                        <View>
                            <Image source={{uri:'https://picsum.photos/500/500'}} style={styles.ModalImage} />
                        </View>

                        <View style={{marginTop:24}}>

                            <View style={styles.InputWrapper}>
                                <TextInput placeholder="Name"  placeholderTextColor='black' style={styles.input}/>
                            </View>

                            <View style={styles.InputWrapper}>
                                <TextInput placeholder="Email"  placeholderTextColor='black' style={styles.input}/>
                            </View>

                            <TouchableOpacity activeOpacity={0.6} style={[styles.voteButton, {width:width/3, paddingVertical:10, alignItems: 'center', alignSelf: 'center'}]} onPress={() => setModalVisible(!modalVisible)}>
                                <Text style={{color: theme.whiteColor}}>Submit</Text>
                            </TouchableOpacity>

                        </View>                               

                    </View>
                    
                </Modal>
 */}



        </>
    )
}

const styles = StyleSheet.create({
    container:{
        // paddingHorizontal: 10,
        flex: 1,
        // marginTop:-50
    },
    VideoPoster:{
        height:130,
        width:'100%',
        borderRadius:18,
    },
    MainWrapper:{
        marginTop:50,
        paddingHorizontal:14,
    },
    contetsHeading:{
        marginTop:4,
        color:'white',
        fontSize:18,
        textAlign: 'center',
    },
    header:
    {
        flexDirection: 'row',
        width: width,
        height: height*0.05,
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginLeft:10,
        padding:10
    },
    headerBtn:{
        width:80.1,
        height:24
    },
    hr:{
        borderWidth:.5,
        borderColor: theme.whiteColor,
        marginTop:20,
    },
    bgImage:{
        height:'100%',
        width:'100%',
    },
    headerImg:{
        width: width,
        // resizeMode: 'center',
        height:200,
    },
    cardWrapper:{
        marginTop:24,
        backgroundColor:theme.whiteColor,
        borderTopRightRadius:20,
        borderTopLeftRadius:20,
    },
    contestanImg:{
        width: 50,
        height:50,
        borderRadius:50
    },
    voteButton:{
        backgroundColor: theme.linkColor,
        borderRadius:4,
        paddingVertical:4,
        paddingHorizontal:14
    },

    addButton:{
        // position: 'relative',
        // right:10,
        // bottom:35,
        borderRadius:10,
        paddingHorizontal:10,
        borderTopRightRadius:50,
        borderBottomRightRadius:50,
        backgroundColor:'#ff6666',
        zIndex:10,
        padding:10,
        width:100
    },


    modalViewHai:{
        backgroundColor: theme.whiteColor,
        paddingVertical:14,
        height:height,
        paddingHorizontal:20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    CardWrapper:{
        width:'44%',
        marginHorizontal: 10,
        marginBottom:20,
        backgroundColor: theme.whiteColor,
        borderRadius: 10,
        paddingVertical: 2,
        alignItems: 'center',
        
    },
    poster:{
        alignSelf: 'center',
        width: 153,
        height:80,
        borderRadius: 10,
    },
    BlogTitle:{
        textAlign: 'center'   ,
        color:'black'
    },
    BlogBody:{
        fontSize:10,
        textAlign: 'center',
        paddingHorizontal:6,
        paddingTop:2,
        paddingBottom:8,
    },
    ModalImage:{
        width:width-40,
        alignSelf: 'center',
        borderRadius:14,
        height:180
    },


    InputWrapper:{
        backgroundColor: '#383838',
        borderWidth:1,
        marginTop:20,
        borderRadius: 40,
        marginBottom:20,
        paddingHorizontal:12,
        paddingVertical:4,
    },
    input:{
        padding:7.5,
        color:'white'
    },

})
