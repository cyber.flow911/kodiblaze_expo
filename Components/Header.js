import React from "react";
import { View, Platform,Text,Dimensions,Image,TouchableOpacity,StyleSheet } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {assets} from './config';
import {SET_AUTH_STATUS,SET_USER_DETAILS} from '../Reducers/types';
import {useDispatch,useSelector} from 'react-redux'
import { AntDesign } from '@expo/vector-icons'; 
import AsyncStorage from '@react-native-async-storage/async-storage';



const height = Dimensions.get('window').height
function Header () {
    const authStatus = useSelector((state)=>state.user.authStatus)
    const props = useSelector((state)=>state.user.props);
    const showHeader = useSelector((state)=>state.user.recordVideo)
    const screenName = props.state?(props.state.routeNames[props.state.index]):('Home');
    const dispatch = useDispatch();

    function logoutUser(){
        AsyncStorage.setItem('user_details', '')  ;
        AsyncStorage.setItem('authStatus', '')  ;
        dispatch({type:SET_AUTH_STATUS,payload:{authStatus:false}})
        dispatch({type:SET_USER_DETAILS,payload:{userDetails:{}}})
    }

  return (
    showHeader?(null):(
    <View style={styles.container} >
        <View style={[styles.header,{marginLeft:screenName=='Home'?(30):(45)}]}> 

            {/* {screenName=='Home'?(null):(
                <TouchableOpacity onPress={()=>props.navigation.goBack(null)}> 
                    <View style={styles.backIcon}>
                        <Image source={assets.home.backIcon} style={{height:20,width:30}} />
                    </View>
                </TouchableOpacity>
            )} */}
            {screenName!='Profile'?(
                <TouchableOpacity onPress={()=>props.navigation.navigate('Profile')}>
                    <View style={styles.searchIcon}>
                        <Image source={assets.home.profileIcon} style={{height:20,width:20}} />
                    </View>
                </TouchableOpacity>
            ):(null)}
            <View style={styles.routeName}>
                <Text style={styles.screenName}>{screenName}</Text>
            </View>
            <TouchableOpacity style={{marginRight:-20}} onPress={()=>logoutUser()}>
                <Text>
                    <AntDesign name="logout" size={24} color="white" />
                </Text>
            </TouchableOpacity>
        </View>
        <View style={{flexDirection:'row',justifyContent:'center'}}>
            <View style={{backgroundColor:'white',height:1,marginTop:10,width:100,marginLeft:screenName=='Home'?0:10}} />
        </View>
    </View>
    )
  );
};
// position: relative;

const styles = StyleSheet.create({

    container: {
        height: height/15,
        marginTop: Platform.OS == "ios" ? 35 : height/20,
        backgroundColor:'black',
        display: 'flex',
        flexDirection: 'column',
        justifyContent:'center',
    },
    header:{
        display:'flex',
        flexDirection: 'row',
        marginTop:10
        
    },
    screenName:{
        color:'white',
        fontSize:20,
    },
    backIcon:{
        display:'flex',
        flex:0.3,
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    routeName:{
        display:'flex',
        flexDirection:'row',
        flex:0.9,
        justifyContent:'center'
    },
    searchIcon:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'flex-end',
        marginRight:-10
    }
})
export default Header;
