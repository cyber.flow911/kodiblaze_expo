import React,{useEffect,useState} from 'react'
import { Image } from 'react-native'
import { StyleSheet, Text, View, Dimensions, ImageBackground,FlatList,ActivityIndicator } from 'react-native'
import { assets, theme,imageBaseUrl } from '../config'
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width
import {fetchContestWinner} from '../../Apis/Contest'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export default function Leaderboard(props) {

    const contestId = props.route.params.contestId;

    const RenderTableData=(item)=>{
        return(
            <View style={{flexDirection:'row',marginLeft:30}}>
                <View style={{flexDirection: 'column',justifyContent:'flex-start',flex:0.6, marginBottom:8}}>
                    <Text style={{color: theme.whiteColor}}>{item.name}</Text>
                </View>
                <View style={{flexDirection: 'column', justifyContent: 'flex-end', marginBottom:8}}>
                    <Text style={{color: theme.whiteColor}}>{item.votes}</Text>
                </View>
            </View>
        )
    }

    const [winnerDetails , setWinnerDetails] = useState({});
    const [contestants , setContestants] = useState({});
    const [loader,setLoader] = useState(false);

    useEffect(() => {
        fetchContestWinner(contestId,winnerCallback);
        setLoader(true);
    },[contestId])

    function winnerCallback(response){
        if(response.msg == 'success')
        {
            setWinnerDetails(response.winner);
            setContestants(response.contestants);
        }
        setLoader(false);
    }



    return (
        <>
            <ImageBackground source={assets.leaderboard.leaderBoardBg} style={styles.backgroundImg}>
                {loader?
                (
                    <View style={[styles.container,{marginTop:-50}]}>
                            <ActivityIndicator size="large"  color="white" style={{marginTop:height/2.2}} />
                    </View>
                )
                :(<>
                <View style={{flexDirection: 'row', marginTop:20, alignItems: 'center', justifyContent: 'space-around', paddingHorizontal:40, alignItems: 'center'}}>
                    {/* <View>
                        <Image source={assets.leaderboard.secondRank} style={styles.secondRankImg} />
                        <Text style={styles.rankName}>Joh McGrown</Text>
                    </View> */}
                    <View style={{alignItems: 'center'}}>
                        
                        {winnerDetails?(
                            <>
                            <Image style={styles.firstRankImg}
                            source={winnerDetails.profilePic?(winnerDetails.profilePic.includes('https')||winnerDetails.profilePic.includes('http')?({uri:winnerDetails.profilePic}):({uri:imageBaseUrl+winnerDetails.profilePic})):(assets.leaderboard.firstRank)}
                            />
                            <Text style={styles.rankName}>{winnerDetails.name}</Text>
                            </>
                        ):(
                            null
                        )}
                        
                    </View>
                    {/* <View>
                        <Image source={assets.leaderboard.thirdRank} style={styles.secondRankImg} />
                        <Text style={styles.rankName}>Lance Garfields</Text>
                    </View> */}
                </View>

                <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-around', marginTop:60, marginBottom:40,}}>
                    {/* <View>
                        <Text style={{fontSize:18, color: theme.whiteColor}}>23</Text>
                    </View> */}
                    {winnerDetails?(
                    <>
                        <View style={{alignItems:'center'}}>
                            <Image 
                                style={{height:50, width:50, borderRadius:50}}
                                source={winnerDetails.profilePic?(winnerDetails.profilePic.includes('https')||winnerDetails.profilePic.includes('http')?({uri:winnerDetails.profilePic}):({uri:imageBaseUrl+winnerDetails.profilePic})):(assets.leaderboard.firstRank)}
                                />
                            <Text style={{fontSize:16, color: theme.whiteColor}}>{winnerDetails.name}</Text>
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{fontSize:20, color: theme.whiteColor}}><MaterialCommunityIcons name="vote" size={34} color="white" /> {winnerDetails.votes}</Text>
                            <Text style={{fontSize:16, color: theme.whiteColor}}>Total votes</Text>
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{fontSize:20, color: theme.whiteColor}}>{winnerDetails.videos}</Text>
                            <Text style={{fontSize:16, color: theme.whiteColor}}>Videos</Text>
                        </View>
                    </>
                    ):(null)}
                </View>

                {contestants!='notFound'?(
                    <FlatList
                        data={contestants}
                        renderItem={({item,index})=>RenderTableData(item,index)}
                        keyExtractor={(item)=>item.id}
                        horizontal={false}
                        
                    />
                ):(
                    <Text style={{color: theme.whiteColor,marginLeft:20}}>No Contestants!</Text>
                )}
                </>)}
            </ImageBackground>
        </>
    )
}

const styles = StyleSheet.create({
    backgroundImg:{
        // width:width,
        // height:height,
        // marginTop:34,
        flex:1,
        width:width+4,
        alignSelf: 'center'
    },
    secondRankImg:{
        height:60,
        width:60,
        resizeMode:'center'
    },
    firstRankImg:{
        height:80,
        width:80,
        resizeMode:'center'
    },
    rankName:{
        color: theme.whiteColor,
        width:60,
        textAlign:'center',
        marginTop:6,
        fontSize:13
    }
})
