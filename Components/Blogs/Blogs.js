import React, { useState,useEffect } from 'react'
import { Text, StyleSheet, View, ImageBackground,TouchableOpacity, Image, FlatList,Dimensions } from 'react-native'
import {assets, theme , imageBaseUrl} from '../config'
import { Ionicons } from '@expo/vector-icons';
import {fetchAllBlogs} from '../../Apis/Blog';
import Shimmer from '../Utils/Shimmer'

const width = Dimensions.get('window').width
export default function Blogs (props){


        const [blogs , setBlogs] = useState([]);
        const [filter,setfilter] = useState('default');
        const [loader , setLoader] = useState(false);

        useEffect(() => {
            if(filter != '')
            {
                fetchAllBlogs(filter,blogsCallback)
                setLoader(true);
            }
        },[filter]);

        function changeFilter(filter) {
            setfilter(filter);
            setTimeout(() =>{
                setfilter('');
            },2000)
        }

        function blogsCallback(response){
            if(response.msg == 'success')
            {
                setBlogs(response.blogs);
                setLoader(false);
            }
        }

   

    const renderBlogList=(item)=>{
        return(
            <TouchableOpacity style={styles.CardWrapper} onPress={()=>{props.navigation.navigate('BlogPage',{blogId:item.id})}}>
               <View >
                    <View>
                        <Image source={item.image.includes('https')?({uri:item.image}):({uri:imageBaseUrl+item.image})} style={styles.poster}/>
                    </View>
                    <View>
                        <Text style={styles.BlogTitle}>{item.name}</Text>
                        <Text style={styles.BlogBody}>{item.short_des}</Text>
                    </View>
               </View>
            </TouchableOpacity>
        )
    }


        return (
            <View styles={styles.container}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
                

            <View style={styles.HeaderButtonWrapper}>
                    <TouchableOpacity onPress={()=>{changeFilter('latest')}}>
                        <Image source={assets.video.btnLatest} style={styles.headerBtn} />
                    </TouchableOpacity >
                    <TouchableOpacity onPress={()=>{changeFilter('random')}}>
                        <Image source={assets.video.colorBorder} style={styles.headerBtn} />
                        <View style={{backgroundColor:'black'}}>
                            <Text style={[{fontSize:12,zIndex:10,position: 'relative',bottom:20,left:17,color:'white'}]}>Random</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{changeFilter('oldest')}}>
                        <Image source={assets.video.colorBorder} style={styles.headerBtn} />
                        <View style={{backgroundColor:'black'}}>
                            <Text style={[{fontSize:13,zIndex:10,position: 'relative',bottom:20,left:20,color:'white'}]}>Oldest</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                {loader?(
                    <View style={{marginLeft:20}}>
                        <Shimmer width={width-30} height={120} />
                    </View>
                ):(
                     <FlatList
                     data={blogs}
                     renderItem={({item})=>renderBlogList(item)}
                     keyExtractor={(item)=>item.id}
                     numColumns={2}
                 />  
                )}
                               
            </ImageBackground>
            </View>
            
        )
}

const styles = StyleSheet.create({
        bgImage:{
            height:'100%',
            width:'100%',
        },
        container:{
            paddingHorizontal: 10,
            flex: 1,
        },
        headerbtnWrapper:{
            marginTop:50,
            flexDirection: 'row',
            justifyContent:'space-between',
            paddingHorizontal:60,
            marginBottom:20,
        },
        HeaderButtonWrapper:{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal:30,
            marginTop:50,
            marginBottom:30,
        },
        headerBtn:{
            width:80.1,
            height:24
        },
        headerbtnflex:{
            flexDirection: 'row',
            alignItems:'center',
        },
        headerbtnText:{
            color: theme.whiteColor,
            marginLeft:6
        },


            CardWrapper:{
            width:'44%',
            marginHorizontal: 10,
            marginBottom:20,
            backgroundColor: theme.whiteColor,
            borderRadius: 10,
            paddingVertical: 2,
            alignItems: 'center',
            
        },
        poster:{
            alignSelf: 'center',
            width: 153,
            height:80,
            borderRadius: 10,
        },
        BlogTitle:{
            textAlign: 'center'   
        },
        BlogBody:{
            fontSize:10,
            textAlign: 'center',
            paddingHorizontal:6,
            paddingTop:2,
            paddingBottom:8,
        }
})
