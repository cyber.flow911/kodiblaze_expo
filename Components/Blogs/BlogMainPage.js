import React,{useState, useEffect} from 'react'
import { Dimensions, FlatList, Image, Modal, StyleSheet,TouchableWithoutFeedback,ImageBackground,ActivityIndicator, Text, TextInput, TouchableOpacity, View,ScrollView } from 'react-native'
import { assets, theme,imageBaseUrl,videoBaseUrl } from '../config';
import { Ionicons,FontAwesome5 } from '@expo/vector-icons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import CardView from '../Utils/CardView';
import {fetchSingleBlog} from '../../Apis/Blog'
import {useDispatch , useSelector} from 'react-redux'
import {Video} from 'expo-av'
import {SET_VIDEO} from '../../Reducers/types'
import SuccessModal from '../Utils/SuccessModal'
import { WebView } from 'react-native-webview';
import RenderHtml from 'react-native-render-html';
// import WebView from 'react-native-WebView'
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width


export default function BlogMainPage(props) {

    const blogId = props.route.params.blogId;
    const dispatch = useDispatch();
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]; 
    const [loader,setLoader] = useState(false);
    const [blogDetails,setBlogDetails] = useState(false)

    useEffect(() => {
        fetchSingleBlog(blogId,blogCallback);
        setLoader(true);
    },[blogId])

    function blogCallback(response) {
        if(response.msg == "success"){
            setBlogDetails(response.data);
            setLoader(false);
        }
    }

    
    Date.createFromMysql = function(mysql_string)
    { 
        var t, result = null;

        if( typeof mysql_string === 'string' )
        {
            t = mysql_string.split(/[- :]/);

            //when t[3], t[4] and t[5] are missing they defaults to zero
            result = new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);          
        }

        return result;   
    }

    function generateDate(date) {
        var timeToDate = Date.createFromMysql(date);
        var returedDate = timeToDate.getDate() + ' ' + months[timeToDate.getMonth()] + ' ' + (-100+timeToDate.getYear()); 
        return returedDate;
    }


    return (
        <>
      {  loader?(
          <View style={[styles.container,{marginTop:-50}]}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
                <ActivityIndicator size="large"  color="white" style={{marginTop:height/2.2}} />
            </ImageBackground>
        </View>
      ):(<View style={[styles.container,{marginTop:-50}]}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
            <TouchableOpacity onPress={()=>{props.navigation.navigate('Blogs')}} style={{position: 'relative',top:60,left:10,zIndex:10,marginBottom:20}}>
                    <FontAwesome5 name="arrow-left" size={20} color={'white'} style={{padding:2}} />
                </TouchableOpacity>
                <View style={styles.MainWrapper}>
                    <Image source={blogDetails?(blogDetails.image.includes('https')||blogDetails.image.includes('http')?({uri:blogDetails.image}):({uri:imageBaseUrl+blogDetails.image})):({uri:'https://picsum.photos/500/500'})} style={styles.VideoPoster} />
                </View>
            {/* <Image source={{uri:'https://picsum.photos/500/500'}} style={styles.headerImg} /> */}
            <View style={{marginTop:14}}>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <Text style={{fontSize:18,color:'white'}}>{blogDetails.name}</Text>
                    <MaterialCommunityIcons name="share" size={24} color="white" />                   
                </View>
                <Text style={{color:'white'}}>
                    {blogDetails.short_des}
                    {/* <WebView
                        originWhitelist={['*']}
                        source={{ html: '<p>Here I am</p>' }}
                    /> */}
                </Text>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop:10}}>
                    <Text style={{color:'white'}}>
                        <MaterialCommunityIcons name="calendar" size={24} color="white" />
                        {blogDetails?(generateDate(blogDetails.timestamp)):(null)}
                    </Text>
                    {CardView(
                        <View style={[styles.container,{marginLeft:10,marginRight:10}]}> 
                            <Image source={assets.video.colorBorder} style={styles.headerBtn} />  
                            <Text style={{color:'white',padding:10,paddingTop:6,position:'relative',bottom:35}}>{blogDetails.category}</Text>
                        </View>,
                        {width: width/3, height: height/25, marginLeft: 'auto', marginRight:5, borderRadius: 30,position:'relative',top:5,opacity:0.96},10,'#303030'
                    )}
                </View>
                <View style={styles.hr}></View>
                {CardView(
                    <ScrollView >
                        <View style={[styles.container,{marginLeft:10,marginRight:10}]}>
                            <View styles={[styles.header,{marginTop:10}]}>
                                <Text style={[styles.contetsHeading,{fontSize:25,textAlign: 'start',margin:20}]}>Description</Text>
                            </View>
                            {blogDetails?(
                                 <RenderHtml 
                                    style={[styles.container,{color:'white'}]}
                                    source={{html:blogDetails.long_description}}
                                 />
                            ):(null)}
                                   
                        </View>
                        </ScrollView>,
                        {width: width, height: height/2, marginLeft: 'auto', marginRight:'auto', borderRadius: 30,position:'absolute',top:height/6,opacity:0.96},10,'#bfbfbf'
                    )}

            </View>
            </ImageBackground>
        </View>
        )}
        </>
    )
}

const styles = StyleSheet.create({
    container:{
        // paddingHorizontal: 10,
        flex: 1,
    },
    VideoPoster:{
        height:130,
        width:'100%',
        borderRadius:18,
    },
    headerBtn:{
        width:width/3,
        height:height/21.5,
        marginLeft:-10,
        marginTop:-5
    },
    MainWrapper:{
        marginTop:50,
        paddingHorizontal:14,
    },
    contetsHeading:{
        marginTop:4,
        color:'black',
        fontSize:18,
        textAlign: 'center',
    },
    header:
    {
        flexDirection: 'row',
        width: width,
        height: height*0.05,
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginLeft:10,
        padding:10
    },
    hr:{
        borderWidth:.5,
        borderColor: theme.whiteColor,
        marginTop:20,
    },
    bgImage:{
        height:'100%',
        width:'100%',
    },
    headerImg:{
        width: width,
        // resizeMode: 'center',
        height:200,
    },
    cardWrapper:{
        marginTop:24,
        backgroundColor:theme.whiteColor,
        borderTopRightRadius:20,
        borderTopLeftRadius:20,
    },
    contestanImg:{
        width: 50,
        height:50,
        borderRadius:50
    },
    voteButton:{
        backgroundColor: theme.linkColor,
        borderRadius:4,
        paddingVertical:4,
        paddingHorizontal:14
    },

    addButton:{
        // position: 'relative',
        // right:10,
        // bottom:35,
        borderRadius:10,
        paddingHorizontal:10,
        borderTopRightRadius:50,
        borderBottomRightRadius:50,
        backgroundColor:'#ff6666',
        zIndex:10,
        padding:10,
        width:100
    },


    modalViewHai:{
        backgroundColor: theme.whiteColor,
        paddingVertical:14,
        height:height,
        paddingHorizontal:20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    CardWrapper:{
        width:'44%',
        marginHorizontal: 10,
        marginBottom:20,
        backgroundColor: theme.whiteColor,
        borderRadius: 10,
        paddingVertical: 2,
        alignItems: 'center',
        
    },
    poster:{
        alignSelf: 'center',
        width: 153,
        height:80,
        borderRadius: 10,
    },
    BlogTitle:{
        textAlign: 'center'   ,
        color:'black'
    },
    BlogBody:{
        fontSize:10,
        textAlign: 'center',
        paddingHorizontal:6,
        paddingTop:2,
        paddingBottom:8,
    },
    ModalImage:{
        width:width-40,
        alignSelf: 'center',
        borderRadius:14,
        height:180
    },


    InputWrapper:{
        backgroundColor: '#383838',
        borderWidth:1,
        marginTop:20,
        borderRadius: 40,
        marginBottom:20,
        paddingHorizontal:12,
        paddingVertical:4,
    },
    input:{
        padding:7.5,
        color:'white'
    },

})
