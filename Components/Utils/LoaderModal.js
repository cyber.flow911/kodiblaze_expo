import React, { Component ,useState} from 'react';
import { View, Text,StyleSheet,ScrollView,TouchableOpacity,ActivityIndicator,Dimensions,Image,TouchableWithoutFeedback, Modal, TextInput, ImageBackground } from 'react-native';
import CardView from './CardView';

const width = Dimensions.get('window').width
const height = Dimensions.get('screen').height

export default function LoaderModal (props) 
{
    const {showModal,setModalVisible ,displayTitle} = props;
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={showModal}
            onRequestClose={()=>setModalVisible(false)}>
                <TouchableWithoutFeedback style={{backgroundColor: 'transparent',opacity:0.1}}>
                    {CardView(
                        <>
                            {displayTitle?(<View style={{flexDirection:'row',justifyContent:'center'}}>
                                <Text style={{color:'black',fontSize:18,marginTop:10,marginBottom:-10}}>{displayTitle}</Text>
                            </View>):(null)}
                            <TouchableWithoutFeedback >
                                <ActivityIndicator size="large"  color="#fe4f5b" style={{marginTop:25}} />
                            </TouchableWithoutFeedback>
                        </>
                    ,{width: width*0.75, height: height/10, marginLeft: 'auto', marginRight:'auto', marginTop:height*0.40}
                    )}
                </TouchableWithoutFeedback>
            </Modal>
        
    )
}

 