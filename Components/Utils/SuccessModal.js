import React, { Component ,useState} from 'react';
import { View, Text,StyleSheet,ScrollView,TouchableOpacity,ActivityIndicator,Dimensions,Image,TouchableWithoutFeedback, Modal, TextInput, ImageBackground } from 'react-native';
import { Feather, Octicons,FontAwesome5 } from '@expo/vector-icons'
import CardView from './CardView';

const width = Dimensions.get('window').width
const height = Dimensions.get('screen').height

export default function SuccessModal (props) 
{
    const {showModal,setModalVisible ,displayTitle,navigateToHome} = props;
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={showModal}
            onRequestClose={()=>setModalVisible(false)}>
                <TouchableWithoutFeedback style={{backgroundColor: 'transparent',opacity:0.1}}>
                    {CardView(
                        <>
                            <View style={{flexDirection:'row',justifyContent:'center',paddingTop:15,backgroundColor:'#3ABA6F'}}>
                                <FontAwesome5 name="check-circle" size={24} color="white" />                            
                            </View>
                            <View style={{flexDirection:'row',flex:0.7,justifyContent:'center',backgroundColor:'#3ABA6F'}}>
                                <Text style={{color:'white',fontSize:18,marginTop:10}}>{displayTitle}</Text>
                            </View>
                            <View style={{flexDirection:'row',flex:0.7,justifyContent:'center'}}>
                                <TouchableOpacity onPress={()=>navigateToHome()} style={{backgroundColor:'#ff6666',marginTop:10,marginBottom:10,padding:5,paddingLeft:20,paddingRight:20,borderRadius:19}}>
                                    <Text style={{color:'white',fontSize:18}}>Close</Text>
                                </TouchableOpacity>
                            </View>
                        </>
                    ,{width: width*0.75, height: height/6, marginLeft: 'auto', marginRight:'auto', marginTop:height*0.40}
                    )}
                </TouchableWithoutFeedback>
            </Modal>
        
    )
}

 