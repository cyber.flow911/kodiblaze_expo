import React, { Component,useEffect,useState } from 'react'
import { Text, StyleSheet, View, Image, ImageBackground,ActivityIndicator, ScrollView,TouchableWithoutFeedback,TouchableOpacity,FlatList,Dimensions } from 'react-native'
import {assets, theme,imageBaseUrl,songBaseUrl,videoBaseUrl,} from '../config'
import { Ionicons } from '@expo/vector-icons';
import {useDispatch,useSelector} from 'react-redux'
import {fetchUserSongs,fetchUserContests,fetchUserVideos,uploadImageToServer,fetchUserDetails} from '../../Apis/Profile'
import {SET_VIDEO,SET_USER_DETAILS} from '../../Reducers/types';
import { Audio, Video } from 'expo-av';
import * as FileSystem from 'expo-file-system';
import * as ImagePicker from 'expo-image-picker';
import Shimmer from '../Utils/Shimmer'



const width = Dimensions.get('window').width
export default function Profile(props) {

    const userDetails = useSelector((state)=>state.user.userDetails)
    const [songs , setUserSongs] = useState({});
    const [videos , setUserVideos] = useState({});
    const [contest , setUserContests] = useState({});
    const [isPlaying , setIsPlaying] = useState(false);
    const [playingSongId , setPlayingSongId] = useState(null);
    const [sound, setSound] = useState();
    const [imageUri , setImageUri] = useState('')
    const [imageLoading, setImageLoading] = useState(false)
    const dispatch = useDispatch();
    const [userImage , setUserImage] = userDetails.profilePic.includes('https')?(useState(userDetails.profilePic)):(useState(imageBaseUrl+userDetails.profilePic));

    const [videosLoader , setVideosLoader] = useState(false);
    const [contestLoader , setContestLoader] = useState(false);
    const [songsLoader , setSongsLoader] = useState(false);

     //fetch user other details
     useEffect(() => {
        fetchUserSongs(userDetails.id,userSongsCallback)
        fetchUserContests(userDetails.id,userContestsCallback)
        fetchUserVideos(userDetails.id,videosCallback);
        setSongsLoader(true);
        setContestLoader(true);
        setVideosLoader(true);
     },[userDetails]);

    useEffect(() => {
        if(userDetails.profilePic.includes('https'))
            setUserImage(userDetails.profilePic)
        else
            setUserImage(imageBaseUrl+userDetails.profilePic)
    },[userDetails])

       //   function to pause song on navigation change
    useEffect( () => {
        const unsubscribe = props.navigation.addListener('blur', async() => {
          await sound.playAsync();
          setIsPlaying(false);
        });
        return unsubscribe; // will be called when the component unmount occurs
      }, [props.navigation]);
    

    const saveFile = async (fileUri) => {

        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status === 'granted') {
          const asset = await MediaLibrary.createAssetAsync(fileUri);
          await MediaLibrary.createAlbumAsync('<FOLDER NAME>', asset, false);
        }
      };
       const fileDownLoad = async (song) => {
      
          const songUrl = songBaseUrl + song.song
          let lastIndex = media?.uri.lastIndexOf('/');
          let fileName = media?.uri.substring(lastIndex);
          let downloadResumable = FileSystem.createDownloadResumable(
            "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
            FileSystem.documentDirectory + fileName,
            {},
            () => {
              // down progress monitor here
            }
          );
          try {
            const { uri } = await downloadResumable.downloadAsync().then((item) => {
              return item;
            });
            await saveFile(uri)
              .then((rs) => {
                alert('Video saved to download folder');
              });
          } catch (e) {
            console.error(e)
          }
        };

        // async function downloadSong(song){
        //     const fileUri: string = `${FileSystem.documentDirectory}${filename}`;
        //     const downloadedFile: FileSystem.FileSystemDownloadResult = await FileSystem.downloadAsync(uri, fileUri);

        //     if (downloadedFile.status != 200) {
        //     handleError();
        //     }
        // }

    const check = async () => 
    {
        if (Platform.OS !== 'web') 
        {
            const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== 'granted') 
            {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
            else
            {
                pickImage();
            }
        }
    }
    
    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [1,1],
          quality: 1,
        });
        
        if(result.uri != null)
        {
            // this.setState({localUri : result.uri})
            setImageUri(result.uri);
            setImageLoading(true);
            // this.setState({imageLoading:true})
            let filename = result.uri.split('/').pop();
            let match = /\.(\w+)$/.exec(filename);
            let type = match ? `image/${match[1]}` : `image`;
            uploadImageToServer(result.uri,filename,type,userDetails.id,uploadImageCallback)
        }
    };

    function uploadImageCallback(response){
        setImageLoading(false);
        if(response.msg == "ok")
        {
            fetchUserDetails(userDetails.id,setUserDetailsCallback)
        }
    }

    function setUserDetailsCallback(response){
        dispatch({type:SET_USER_DETAILS,payload:{userDetails:response.data}})
    }
    

    async function playSound(song) {

        const songUrl = songBaseUrl + song.song
        const { sound } = await Audio.Sound.createAsync(
                {uri:songUrl}
            );
        setPlayingSongId(song.songId);
        setIsPlaying(true);
        setSound(sound);

        await sound.playAsync(); 
    }

    useEffect(() => {
        return sound
        ? () => {
            sound.unloadAsync(); }
        : undefined;
    }, [sound]);

    async function pauseSound() {
        const data = await sound.getStatusAsync()
        setIsPlaying(false);
        await sound.pauseAsync(); 
    }

   

    // stores user video details
    function videosCallback(response) {
        if(response.msg == 'success'){
            setUserVideos(response.data);
        }
        setVideosLoader(false);
    }

    //store user contest details
    function userContestsCallback(response)
    {
        if(response.msg == 'success'){
            setUserContests(response.data);
        }
        setContestLoader(false);
    }

    //stores the songs of the user in songs variable
    function userSongsCallback(response){
        if(response.msg == 'success'){
            setUserSongs(response.data);
        }
        setSongsLoader(false);
    }


    function playVideo(item)
    {
        dispatch({type:SET_VIDEO,payload:{recordVideo:true}})
        props.navigation.navigate('PlayRecordedVideo', { videoUrl:videoBaseUrl+item.video,recordedVideo:true,navigate:'Profile' });
    }

    function renderVideoCard(item){
        return(
            <TouchableOpacity onPress={()=>playVideo(item)}> 
                <View style={styles.videoCardWrapper}>
                    {/* <Image source={assets.profile.video} style={styles.VideoPoster} /> */}
                    <Video 
                        style={styles.VideoPoster}
                        source={{
                            uri: videoBaseUrl+item.video,
                        }}
                        useNativeControls={false}
                        resizeMode="cover"
                    />
                    
                </View>
            </TouchableOpacity>
        )
    }

    function renderContestCard(item){
        return (
            <TouchableOpacity onPress={() =>{props.navigation.navigate('ContestPage',{contestId: item.id})}}>
                <ImageBackground source={assets.contest.WallPrimaryWrapper} style={styles.contestGradientWrapper}>
                    <View style={styles.GradientInnerWrapper}>
                        <ImageBackground source={assets.contest.WallCard} style={styles.contestWall}>
                                <View>
                                    <Image source={{uri:item.header_image}} style={styles.contestImg} />
                                </View>
                                <View style={styles.contestBodyWrapper}>
                                    <Text style={styles.contetsHeading}>{item.name}</Text>
                                    <Text style={styles.contestBody}>{item.description}</Text>
                                </View>
                        </ImageBackground>
                        <View style={{marginLeft:24}}>
                            {/* <View>
                                <Ionicons name="heart-outline" size={24} color="white" />
                            </View> */}
                            <View style={{marginTop:14}}>
                                <Ionicons name="arrow-forward" size={24} color="white" />
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        )
    }

    function renderSongCard(item) {
        return (

            <View style={{display: 'flex', flex: 1,flexDirection: 'row',margin:10}}>
                <Image source={assets.profile.songImage} style={{height:60,width:60}} />
                <View style={styles.contestBodyWrapper}>
                    <Text style={[styles.contetsHeading,{fontSize:18}]}>{item.name}</Text>
                    <Text style={[styles.contestBody,{fontSize:12}]}>{item.downloads} Downloads  ${item.price}</Text>
                </View>
                {/* <TouchableOpacity style={{position: 'absolute',right:60}} 
                // onPress={() => {fileDownLoad(item)}}
                >
                    <Ionicons name="download-outline" size={30} color="white" />
                </TouchableOpacity> */}
                <TouchableOpacity style={{position: 'absolute',right:25}} 
                onPress={() => {isPlaying&&playingSongId==item.songId?(pauseSound()):(playSound(item))}} >
                    <Ionicons name={isPlaying&&playingSongId==item.songId ? 'pause-outline' : 'play-outline'} size={30} color="white" />
                </TouchableOpacity>              
            </View>
        )
    }

        return (
            <View styles={styles.container}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
            

                <View style={styles.Header}>
                    {imageLoading?(
                        <ActivityIndicator  size="large" color="white"/>
                    ):(
                        <>
                            <View>
                                <Image source={{uri:userImage}} style={{height:75,width:75,borderRadius:50}} />
                            </View>
                            <TouchableOpacity style={{position: 'absolute',bottom:-15}} onPress={() =>check()}>
                                <View>
                                    <Image source={assets.profile.editIcon} style={{height:20,width:20}} />
                                </View>
                            </TouchableOpacity>
                        </>
                    )}       
                </View>
                <View style={[styles.Header,{marginTop:0}]}>
                        <Text style={{color:'white',fontSize:17}}>{userDetails.name}</Text>
                </View>
                <View style={{marginBottom:20,marginLeft:10}}>
                    <Text style={{color:'white',fontSize:20,fontWeight:'500'}}>My Videos</Text>
                </View>
                <View>
                    {videosLoader?(
                        <View style={{marginLeft:20}}>
                            <Shimmer width={width-30} height={80} />
                        </View>
                    ):
                        (videos?(
                            <FlatList
                                horizontal
                                data={videos} 
                                renderItem={({item})=>renderVideoCard(item)} 
                                style={{height:100,width:'auto'}}
                        />):(
                            <Text style={{color:'white',fontSize:15,marginLeft:20}}>No Videos</Text>
                    ))}
                </View>
                <View style={{marginBottom:20,marginTop:20,marginLeft:10}}>
                    <Text style={{color:'white',fontSize:20,fontWeight:'500'}}>Joined Contests</Text>
                </View>
                <View >
                    {contestLoader?(
                        <View style={{marginLeft:20}}>
                            <Shimmer width={width-30} height={120} />
                        </View>
                    ):
                        (contest?(
                            <FlatList
                                horizontal
                                data={contest} 
                                renderItem={({item})=>renderContestCard(item)} 
                                style={{height:150,width:'auto'}}
                    />):(
                        <Text style={{color:'white',fontSize:15,marginLeft:20,marginBottom:20}}>No Contests</Text>
                    ))}
                </View>
                <View style={{marginBottom:20,marginLeft:10}}>
                    <Text style={{color:'white',fontSize:20,fontWeight:'500'}}>My Songs</Text>
                </View>
                <View>
                    {
                        songsLoader?(
                            <View style={{marginLeft:20}}>
                                <Shimmer width={width-30} height={60} />
                            </View>
                        ):
                        (songs?(
                            <FlatList
                                vertical
                                data={songs} 
                                renderItem={({item})=>renderSongCard(item)} 
                                style={{height:200,width:'auto'}}
                        />):(
                            <Text style={{color:'white',fontSize:15,marginLeft:20}}>No Songs</Text>
                        ))
                    }
                </View>
                
            
            </ImageBackground>
            </View>
        )
    }

const styles = StyleSheet.create({
    bgImage:{
        height:'100%',
        width:'100%',
    },
    container:{
        paddingHorizontal: 10,
        flex: 1,
        backgroundColor:'rgba(255,0,0,0.5)',
    },
    Header:{
        flexDirection: 'row',
        // flex:0.5,
        justifyContent: 'center',
        marginTop:30,
        marginBottom:30,
    },
    headerBtn:{
        width:80.1,
        height:24
    },
    headerBtnRecomended:{
        height:24,
        width:95.8,
    },
    videoCardWrapper:{
        marginBottom:40,
        paddingHorizontal:20,
    },
    VideoPoster:{
        height:75,
        width:75,
        // width:'100%',
        borderWidth:2,
        borderRadius:18,
        borderColor:'red',
        // padding: 10
    },
    videoTitle:{
        color: theme.whiteColor,
        fontSize:18,
        fontWeight: 'bold',
    },
    videoDescription:{
        color: theme.whiteColor
    },
    VideoContentWrapper:{
        marginTop:6,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    contestGradientWrapper:{
        marginBottom:30,
        marginLeft:10,
        height:116.6,
        width:328.33,
    },
    GradientInnerWrapper:{
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
    },
    contestWall:{
        width:250,
        height:110,
        flexDirection: 'row',
        // justifyContent: 'space-between',
    },
    contestImg:{
        marginTop:10,
        marginLeft:8,
        width:80,
        height:90,
        borderRadius:10,
    },
    contestBodyWrapper:{
        width:170,
        paddingHorizontal:16,
    },
    contetsHeading:{
        marginTop:4,
        color:theme.whiteColor,
        fontSize:18,
        textAlign: 'center',
    },
    contestBody:{
        marginTop:4,
        color:theme.whiteColor,
        fontSize:10,
        textAlign: 'center',
    }
})
