import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import ContestMainPage from './Contest/ContestMainPage'
import Leaderboard from './Leaderboard/Leaderboard'
import Navigator from './Navigator'
export default function AppStartPoint () {
    
        return (
            <>
            {/* <Leaderboard /> */}
            {/* <ContestMainPage /> */}
            <Navigator />

            </>
        )
}

const styles = StyleSheet.create({})
