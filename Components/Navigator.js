import React, { useEffect, useState } from 'react' 
import Auth from './Auth/Auth'
import Blogs from './Blogs/Blogs'
import Contest from './Contest/Contest'
import Home from './Home/Home'
import Video from './Video/Video'
import VideoView from './Video/VideoView'
import RecordVideo from './Video/RecordVideo'
import Profile from './Profile/Profile'
import Songs from './Songs/Songs'
import {  useDispatch, useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {fetchUserDetails} from '../Apis/Profile'
import TabBar from './TabBar'
import { SET_AUTH_STATUS,SET_USER_DETAILS,SET_PROPS} from '../Reducers/types'
import { ActivityIndicator, View } from 'react-native'
import SplashScreen from 'react-native-splash-screen';
import { getHeaderTitle } from '@react-navigation/elements';
import Header from './Header'
import PlayVideo from './Video/PlayVideo'
import PlayRecordedVideo from './Video/PlayRecordedVideo'
import Leaderboard from './Leaderboard/Leaderboard'
import SelectContest from './Home/SelectContest'
import ContestMainPage from './Contest/ContestMainPage'
import BlogMainPage from './Blogs/BlogMainPage'

// import { SafeAreaView } from 'react-native-safe-area-context';

const Tab = createBottomTabNavigator();
const checkAuth = async (setCheckingAuth,setUserAuthStatus,setUserDetails) => {
    try {
      const value = await AsyncStorage.getItem('authStatus')
      const userData = await AsyncStorage.getItem('user_details');
      if(value != null) {
        // value previously stored
        setUserAuthStatus(true)
        if(userData != null) {
            let data = JSON.parse(userData)
            fetchUserDetails(data.id,setUserDetails)
        }
      }else
      {
        setUserAuthStatus(false);
      }
    
      //updating check auth progress variable  to false so that content can be rendered on screens
      setCheckingAuth(false)
    } catch(e) {
      // error reading value
    }
  }
  
function Navigator(props) {

    const [checkingAuth,setCheckingAuth] = useState(true)
    const authStatus =    useSelector(state=>state.user.authStatus)
    const dispatch = useDispatch();
    const [videoScreen , setVideoScreen] = useState(false);
    const recordVideo = useSelector((state)=>state.user.recordVideo);

    //function to update auth status  of user in redux
    const setUserAuthStatus = (authStatus)=>
    {
        dispatch({type:SET_AUTH_STATUS,payload:{authStatus:authStatus}})
    }

    //function to update user details  of user in redux
    const setUserDetails = (userDetails)=>
    {
        dispatch({type:SET_USER_DETAILS,payload:{userDetails:userDetails.data}})
    }

    //use effec to check uer is logged in or not and if yes then fetch its appointment and update redux accordingly
    useEffect(() => {
        //passing all reedux update fucntion defined above here as parameters 
        checkAuth(setCheckingAuth,setUserAuthStatus,setUserDetails)
    },[])

    useEffect(() => {
        dispatch({type:SET_PROPS,payload:{props:props}})
    },[props]);

    return (
        <>
        <NavigationContainer>
            
        {authStatus?(
                recordVideo?(null):(
                    <Header props={props}/>
                )
                    ):(null)}

            {checkingAuth?(
                <View style={{alignItems: 'center',justifyContent: 'center',flex:1}}>
                    <ActivityIndicator size="large" /> 
                </View>
            ):(
                
                authStatus?(
                    recordVideo?(null):(
                        <Header props={props}/>
                    ),
                    <Tab.Navigator  screenOptions={{ headerShown:false}}
                        tabBar={props => <TabBar {...props} authStatus={authStatus} setVideoScreen={setVideoScreen}/>}
                    >
                        <>
                            <Tab.Screen name="Home" component={Home} />
                            <Tab.Screen name="Contest" component={Contest} />
                            <Tab.Screen name="Record" showHeader={false}  component={RecordVideo} />
                            <Tab.Screen name="Blogs" component={Blogs} />
                            <Tab.Screen name="BlogPage" component={BlogMainPage} />
                            <Tab.Screen name="Videos" component={Video} />
                            <Tab.Screen name="VideoView" component={VideoView} />
                            <Tab.Screen name="Profile" component={Profile} />
                            <Tab.Screen name="Songs" component={Songs} />
                            <Tab.Screen name="SelectContest" component={SelectContest} />
                            <Tab.Screen name="ContestPage" component={ContestMainPage} />
                            <Tab.Screen name="PlayVideo" component={PlayVideo} />
                            <Tab.Screen name="Winner" component={Leaderboard} />
                            <Tab.Screen name="PlayRecordedVideo" showHeader={false} component={PlayRecordedVideo} />
                        </>
                    </Tab.Navigator>
                ):(
                    <Tab.Navigator screenOptions={{ headerShown: false }}
                        tabBar={props => <TabBar {...props} authStatus={authStatus} setVideoScreen={setVideoScreen}/>}
                    >
                        <Tab.Screen name="Auth" component={Auth} />
                    </Tab.Navigator>
            ))}
                
        </NavigationContainer>
        </>
    )
}

export default Navigator
