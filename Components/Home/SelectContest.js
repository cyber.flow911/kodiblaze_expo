import React, { useEffect, useState} from 'react';
import { View, Text,StyleSheet,ScrollView,TouchableOpacity,Dimensions,Image,TouchableWithoutFeedback, Modal, FlatList, ImageBackground,Linking,BackHandler } from 'react-native';
import { Ionicons, Octicons,FontAwesome5 } from '@expo/vector-icons'
import {useDispatch,useSelector} from 'react-redux'
import DropDownPicker from 'react-native-dropdown-picker'
import CardView from '../Utils/CardView';
import {assets,theme} from '../config'
import {SET_VIDEO,SET_CONTEST_ID} from '../../Reducers/types';
import {fetchOngoingContests,checkContestant} from '../../Apis/Contest'
import Shimmer from '../Utils/Shimmer';

const width = Dimensions.get('window').width
const height = Dimensions.get('screen').height

export default function SelectContest (props) {
    
    const {modalVisible,setModalVisible,openModal,closeModal} = props;
    const dispatch = useDispatch()
    const [contest , setContests] = useState([])
    const userDetails = useSelector((state)=>state.user.userDetails);
    const [contestLoader , setContestLoader] = useState(true)


    BackHandler.addEventListener('hardwareBackPress', ()=>{props.navigation.navigate("Home")});
    function enableVideoScreen(){
        dispatch({type:SET_VIDEO,payload:{recordVideo:true}})
        props.navigation.navigate("Record");
        setModalVisible(false)
    }

    useEffect(()=>{
        fetchOngoingContests(setOngoingContests)
    },[])

    function setOngoingContests(response)
    {
        if(response.msg == 'success')
        {
            setContests(response.contests);
            setContestLoader(false)
        }
    }

    function selectContest(id)
    {
        checkContestant(userDetails.id,id,(response)=>checkCallback(response,id))
        
    }

    function checkCallback(response,id){
        if(response.msg == 'notFound')
        {
            dispatch({type:SET_CONTEST_ID,payload:{contestId:id}})
            dispatch({type:SET_VIDEO,payload:{recordVideo:true}})
            props.navigation.navigate("Record");
            setModalVisible(false)
        }
        else
        {
            alert("Already Participated!");
        }
    }

    function renderContestCard(item){
        return (
            <TouchableOpacity onPress={()=>{selectContest(item.id)}} 
                style={{backgroundColor:'#b3b3b3',marginRight:10,marginLeft:5}}
            >
                <ImageBackground source={assets.contest.WallPrimaryWrapper} style={styles.contestGradientWrapper}>
                    <View style={styles.GradientInnerWrapper}>
                        <ImageBackground source={assets.contest.WallCard} style={styles.contestWall}>
                                <View>
                                    <Image source={{uri:item.header_image}} style={styles.contestImg} />
                                </View>
                                <View style={styles.contestBodyWrapper}>
                                    <Text style={styles.contetsHeading}>{item.name}</Text>
                                    <Text style={styles.contestBody}>{item.description}</Text>
                                </View>
                        </ImageBackground>
                        <View style={{marginLeft:24}}>
                            <View>
                                <Ionicons name="heart-outline" size={24} color="white" />
                            </View>
                            <View style={{marginTop:14}}>
                                <Ionicons name="arrow-forward" size={24} color="white" />
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        )
    }

        return(
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={()=>{
                    setModalVisible(false);
                  }}
            >
                    <TouchableWithoutFeedback onPress={()=> {}} >
                            <View style={{width:'100%',height:'100%'}}>
                                <View>
                                    {CardView(
                                    <TouchableWithoutFeedback>
                                        <View style={styles.container}>
                                            <View style={styles.header}>
                                                <TouchableOpacity onPress={()=>{setModalVisible(false)}} xstyle={{padding:0}}>
                                                    <FontAwesome5 name="arrow-left" size={20} color={'black'} style={{padding:2}} />
                                                </TouchableOpacity>
                                                <Image />
                                            </View>
                                            <View style={{marginLeft:width/17}}>
                                                {contestLoader?(
                                                    <Shimmer width={width-30} height={120} />
                                                ):(
                                                    <FlatList
                                                        data={contest} 
                                                        renderItem={({item})=>renderContestCard(item)} 
                                                        style={{height:height/2.5,width:'auto'}}
                                                    />
                                                )}
                                            </View>            
                                        </View>
                                        </TouchableWithoutFeedback>,
                                        {width: width, height: height/1.75, marginLeft: 'auto', marginRight:'auto', borderRadius: 20,position:'absolute',top:height/2},10,'white'
                                    )}
                                </View>
                            </View>
                    
                    </TouchableWithoutFeedback>
            </Modal>
        )
    }


const styles = StyleSheet.create({
    container: 
    {
        flex: 1,
        flexDirection: 'column',
        width:width,
        height: height,
        margin:'auto', 
        marginLeft:-10,
        // borderWidth: 1
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        // paddingTop:50,
    },
        header:
        {
            flexDirection: 'row',
            width: width,
            height: height*0.05,
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginLeft:10,
            padding:10
        },
            postQueText:
            {
               fontSize: 24,
               marginLeft: '5%',
               marginTop: '2%',
               flexDirection: 'row',
               justifyContent: 'center'
            },
            closeIcon:
            {
                height: height*0.027,
                alignSelf: 'flex-end',
                width: width*0.06,
                marginLeft: 'auto',
                marginBottom: 10, 
                resizeMode: 'contain',
                position: 'relative',
                right:-width*0.07
            },
            contestGradientWrapper:{
                marginBottom:30,
                marginLeft:10,
                height:116.6,
                width:328.33,
                marginTop:10,
            },
            GradientInnerWrapper:{
                flexDirection: 'row',
                // justifyContent: 'space-between',
                alignItems: 'center',
            },
            contestWall:{
                width:250,
                height:110,
                flexDirection: 'row',
                // justifyContent: 'space-between',
            },
            contestImg:{
                marginTop:10,
                marginLeft:8,
                width:80,
                height:90,
                borderRadius:10,
            },
            contestBodyWrapper:{
                width:170,
                paddingHorizontal:16,
            },
            contetsHeading:{
                marginTop:4,
                color:theme.whiteColor,
                fontSize:18,
                textAlign: 'center',
            },
            contestBody:{
                marginTop:4,
                color:theme.whiteColor,
                fontSize:10,
                textAlign: 'center',
            }
})
