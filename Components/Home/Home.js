import React, { Component,useEffect,useState,useRef } from 'react'
import { Text, StyleSheet, Dimensions, View, ImageBackground, Image, FlatList, ScrollView,TouchableOpacity,TouchableWithoutFeedback,ActivityIndicator} from 'react-native'
import {assets, theme,imageBaseUrl,songBaseUrl,videoBaseUrl} from '../config'
import { Ionicons } from '@expo/vector-icons';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { fetchBanner,fetchVideo,fetchSongs,fetchBlogs } from '../../Apis/Home';
import { SET_VIDEO } from '../../Reducers/types';
import { Audio, Video } from 'expo-av';
import * as VideoThumbnails from 'expo-video-thumbnails';
import {useDispatch ,useSelector} from 'react-redux';
import Shimmer from '../Utils/Shimmer'


const { width: screenWidth } = Dimensions.get('window');
const width = Dimensions.get('window').width

export default function Home (props) {

  const video = useRef(null);
  const dispatch = useDispatch();
  const [carouselItems , setCarouselItems] = useState([
      {
          title:"Item 1",
          text: "Text 1",
          url: assets.home.KodiblazeBannerImg
      },
      {
          title:"Item 2",
          text: "Text 2",
          url: assets.home.KodiblazeBannerImg
      },
      {
          title:"Item 3",
          text: "Text 3",
          url: assets.home.KodiblazeBannerImg
      }])
  const [blogs,setBlogs] = useState([]);
  const [videos , setVideos] = useState([]);
  const [songs , setSongs] = useState([]);
  const [isPlaying , setIsPlaying] = useState(false);
  const [playingSongId , setPlayingSongId] = useState(null);
  const [sound, setSound] = useState();
  const [image, setImage] = useState(null);
  const [status,setStatus] = useState({})
  const [videoLoader,setVideoLoader] = useState(true);
  const [songsLoader, setSongsLoader] = useState(true);
  const [blogsLoader, setblogsLoader] = useState(true);

  //fetching home page sections
  useEffect(() => {
    fetchBanner(bannerCallback);
    fetchBlogs(blogsCallback);
    fetchSongs(songsCallback);
    fetchVideo(videoCallback);
  },[])

  //set vidoes
  function videoCallback(response){
    if(response.msg == 'success')
    {
        setVideos(response.videos)
    }
    setVideoLoader(false);
}

  //set songs
  function songsCallback(response){
    if(response.msg == 'success')
    {
        setSongs(response.songs)
    }
    setSongsLoader(false);
  }

  //set blogs
  function blogsCallback(response) {
    if(response.msg == 'success')
    {
        setBlogs(response.blogs);
    }    
    setblogsLoader(false);
  }

  //set banner
  function bannerCallback(response) {
    if(response.msg == 'success')
        setCarouselItems(response.banner)
  }

  const generateThumbnail = async (item) => {
    // setVideoLoader(true)
    const thumbnail = videoBaseUrl+item.video;
    try {
      const { uri } = await VideoThumbnails.getThumbnailAsync(
        thumbnail,
        {
          time: 15000,
        }
      );
    //   setVideoLoader(false)
      setImage(uri);
      // return uri;
    } catch (e) {
      console.warn(e);
    }
    
  };

//   function to pause song on navigation change
    useEffect( () => {
      const unsubscribe = props.navigation.addListener('blur', async() => {
        await sound.playAsync();
        setIsPlaying(false);
      });
      return unsubscribe; // will be called when the component unmount occurs
    }, [props.navigation]);
  
    async function playSound(song) {

      const songUrl = songBaseUrl + song.song
      const { sound } = await Audio.Sound.createAsync(
              {uri:songUrl}
          );
      setPlayingSongId(song.id);
      setIsPlaying(true);
      setSound(sound);

      await sound.playAsync(); 
  }

  useEffect(() => {
      return sound
      ? () => {
          sound.unloadAsync(); }
      : undefined;
  }, [sound]);

  async function pauseSound() {
      const data = await sound.getStatusAsync()
      setIsPlaying(false);
      await sound.pauseAsync(); 
  }


    const _renderItem =({item, index}, parallaxProps) =>{
        return (
            <View style={styles.CarouselItem}>
                <ParallaxImage
                    source={{uri:imageBaseUrl+item.url}}
                    containerStyle={styles.CarouselImageContainer}
                    style={styles.Carouselimage}
                    parallaxFactor={0.2}
                    {...parallaxProps}
                />
            </View>
        );
    }

    function playVideo(item)
    {
        dispatch({type:SET_VIDEO,payload:{recordVideo:true}})
        props.navigation.navigate('PlayRecordedVideo', { videoUrl:videoBaseUrl+item.video,recordedVideo:true,navigate:'Home' })
    }

     const renderVideosList=(item)=>{
    //generateThumbnail(item)
        return(
          (
            <TouchableWithoutFeedback onPress={()=>{playVideo(item)}}>
            <View style={styles.VideoWrapper}>
               <View >
                    <View>
                        <Video
                            // ref={video}
                            style={styles.videoImage}
                            source={{
                            uri: videoBaseUrl+item.video,
                            }}
                            useNativeControls={false}
                            resizeMode="cover"
                            isLooping
                            onPlaybackStatusUpdate={status => setStatus(() => status)}
                        />
                    {/* {videoLoader?(
                        <Image source={{uri:image}} style={styles.videoImage}/>
                        ):(
                        <Image source={assets.blog.blogImg} style={styles.videoImage}/>
                    )} */}
                    </View>
                    <View style={{position:'absolute'}}>
                      <Ionicons name={'play-outline'} size={30} color="white" />
                    </View>
               </View>
            </View>
            </TouchableWithoutFeedback>
          )
        )
    }


    const renderSongsList=(item)=>{
        return(
        <View style={{display: 'flex', flex: 1,flexDirection: 'row',margin:10}}>
            <Image source={assets.profile.songImage} style={{height:60,width:60}} />
            <View style={styles.contestBodyWrapper}>
                <Text style={[styles.contetsHeading]}>{item.name}</Text>
                <Text style={[styles.contestBody]}>{item.downloads} Downloads  ${item.price}</Text>
            </View>
            <TouchableOpacity style={{position: 'absolute',right:0}} 
            onPress={() => {isPlaying&&playingSongId==item.id?(pauseSound()):(playSound(item))}} >
                <Ionicons name={isPlaying&&playingSongId==item.id ? 'pause-outline' : 'play-outline'} size={30} color="white" />
            </TouchableOpacity>              
        </View>
        )
    }

    const renderBlogList=(item)=>{
        return(
            <TouchableOpacity style={styles.BlogCardWrapper} onPress={()=>{props.navigation.navigate('BlogPage',{blogId:item.id})}}>
               <View >
                    <View>
                        <Image source={item.image.includes('https')?({uri:item.image}):({uri:imageBaseUrl+item.image})} style={styles.Blogposter}/>
                    </View>
                    <View>
                        <Text style={styles.BlogTitle}>{item.name}</Text>
                        <Text style={styles.BlogBody}>{item.short_des}</Text>
                    </View>
               </View>
            </TouchableOpacity>
        )
    }


        return (
            <View styles={styles.container}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
            
            <ScrollView>
              <View style={styles.innercontainer}>
               
                    <Text style={styles.h3}>Banner</Text>
            
                    <View>
                        <Carousel
                            sliderWidth={screenWidth}
                            sliderHeight={screenWidth}
                            itemWidth={screenWidth - 190}
                            data={carouselItems}
                            renderItem={_renderItem}
                            hasParallaxImages={true}
                            loop={true}
                            autoplay={true}
                        />
                    </View>

                    

                <View style={styles.headingWrapper}>
                    <Text style={styles.h3}>Videos</Text>
                    <TouchableOpacity onPress={()=>{props.navigation.navigate('Videos')}}>
                        <Ionicons name="arrow-forward" size={24} color="white" />
                    </TouchableOpacity>
                </View>


                    {videoLoader?(
                        <View>
                            <Shimmer width={width} height={120} />
                        </View>
                    ):(
                        <FlatList
                            data={videos}
                            renderItem={({item})=>renderVideosList(item)}
                            keyExtractor={(item)=>item.id}
                            // numColumns={2}
                            horizontal={true}
                            
                        />
                    )}

                <View style={styles.headingWrapper}>
                    <Text style={styles.h3}>Latest Beats</Text>
                    <TouchableOpacity onPress={()=>{props.navigation.navigate('Songs')}}>
                      <Ionicons name="arrow-forward" size={24} color="white" />
                    </TouchableOpacity>
                </View>
                    {songsLoader?(
                        <View>
                            <Shimmer width={width} height={60} />
                        </View>
                    ):(
                        <FlatList
                            data={songs}
                            renderItem={({item})=>renderSongsList(item)}
                            keyExtractor={(item)=>item.id}
                            // numColumns={2}
                            horizontal={true}
                        />
                    )}
                    
                <View style={styles.headingWrapper}>
                    <Text style={styles.h3}>Blogs</Text>
                    <Ionicons name="arrow-forward" size={24} color="white" />
                </View>

                        {blogsLoader?(
                            <Shimmer width={width} height={120} />
                        ):(
                            <FlatList
                                data={blogs}
                                renderItem={({item})=>renderBlogList(item)}
                                keyExtractor={(item)=>item.id}
                                horizontal={true}
                                
                            /> 
                        )}  
              </View>
            </ScrollView>
            </ImageBackground>
            </View>
        )
}

const styles = StyleSheet.create({
    bgImage:{
        height:'100%',
        width:'100%',
    },
    container:{
        flex: 1,
        // marginTop:40
    },
    innercontainer:{
        paddingHorizontal:12,
        paddingTop:40,
    },

    h3:{
        fontSize:18,
        color: theme.whiteColor,
    },
    headingWrapper:{
        marginTop:20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },


    CarouselItem: {
        width: screenWidth - 200,
        height: screenWidth - 150,
        marginTop:14,
      },
      CarouselImageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        // borderRadius: 8,
      },
      Carouselimage: {
        // ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
        // width: 1000,
        // height:'10%',
      },
      contestImg:{
        marginTop:10,
        marginLeft:8,
        width:80,
        height:90,
        borderRadius:10,
    },
    contestBodyWrapper:{
        width:170,
        paddingHorizontal:16,
    },
    contetsHeading:{
        marginTop:4,
        color:theme.whiteColor,
        fontSize:18, 
    },
    contestBody:{
        marginTop:4,
        color:theme.whiteColor,
        fontSize:12, 
    },
      VideoWrapper:{
         paddingTop:16, 
      },
      videoImage:{
          width: screenWidth - screenWidth/1.8,
          height: 120,
          marginRight:8,
          borderRadius:12,
      },
      VideoTitle:{
          color: theme.whiteColor,
          alignSelf: 'center',
          position: 'absolute',
          bottom: 2,
          fontSize:14,
          fontWeight: 'bold',
          overflow: 'hidden',
          paddingLeft:4,
          paddingRight:10,
      },


      BlogCardWrapper:{
        marginTop:16,
        width: screenWidth - screenWidth/1.8,
        marginHorizontal: 5,
        marginBottom:20,
        backgroundColor: theme.whiteColor,
        borderRadius: 10,
        paddingVertical: 2,
        alignItems: 'center',
        
    },
    Blogposter:{
        alignSelf: 'center',
        width: 153,
        height:80,
        borderRadius: 10,
    },
    BlogTitle:{
        textAlign: 'center'   
    },
    BlogBody:{
        fontSize:10,
        textAlign: 'center',
        paddingHorizontal:6,
        paddingTop:2,
        paddingBottom:8,
    }
})
