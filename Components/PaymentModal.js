import React, { Component ,useState,useEffect} from 'react';
import { View, Text,StyleSheet,ActivityIndicator,TouchableOpacity,Dimensions,Image,TouchableWithoutFeedback, Modal, TextInput, ImageBackground,Linking,BackHandler } from 'react-native';
import { Avatar, Badge, Icon, withBadge } from 'react-native-elements';
import { Ionicons, Octicons,FontAwesome5 } from '@expo/vector-icons'
import { WebView } from 'react-native-webview';
import CardView from './Utils/CardView';
import {theme, assets,serverBaseUrl} from './config'
const width = Dimensions.get('window').width
const height = Dimensions.get('screen').height

export default function PaymentModal(props){

    const {modalVisible,setModalVisible,song,userDetails,setPaymentComplete} = props;
    const paymentUrl = props.song?(serverBaseUrl + 'paypal.php?amount='+song.price+'&songId='+song.id+'&userId='+userDetails.id+'&email='+userDetails.email):(null);
    const [details,setDetails] = useState(true)
    const [loader,setLoader] = useState(false)

    useEffect(() => {
        setDetails(true);
        setLoader(true);
        setPaymentComplete(false)
    },[modalVisible])
    //vanshpatpatia@gmail.com
    //12345678
    //amount=10&songId=18&userId=22&email=vansh10patpatia@gmail.com

    function handleMessage(data)
    {
        const message = data.data;
        switch (message)
        {
            case 'displaying':
                setLoader(false);
                break;
            case 'PaymentInitiated':
                setDetails(false);
                break;

            case 'PaymentCompleted':
                setDetails(true);
                setModalVisible(false);
                setPaymentComplete(true);
                break;
        }
    }

    return(
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={()=>{
                setModalVisible(false);
            }}
        >
                <TouchableWithoutFeedback onPress={()=> {}} >
                    <View style={{width:'100%',height:'100%',backgroundColor:theme.translucentColor}}>
                        <View>
                            {CardView(
                                <TouchableWithoutFeedback>
                                    <View style={styles.container}>
                                        <View style={styles.header}>
                                        <TouchableOpacity onPress={()=>{setModalVisible(false)}} xstyle={{padding:0}}>
                                                    <FontAwesome5 name="arrow-left" size={20} color={'black'} style={{padding:2}} />
                                                </TouchableOpacity>
                                        </View>
                                        {details?(
                                            <View style={{ flexDirection: 'row' ,flex:0.25,justifyContent:'center'}}>
                                                <Text  style={[styles.postQueText,{marginTop:-2,flexShrink: 1,marginLeft:10,marginRight:10,fontSize:20}]}>
                                                    Transaction Details
                                                </Text> 
                                                {CardView(
                                                    <View>
                                                        <View style={{display: 'flex',flexDirection: 'row',margin:10}}>
                                                            <Image source={assets.profile.songImage} style={{height:60,width:60}} />
                                                            <View style={[styles.contestBodyWrapper,{marginLeft:10}]}>
                                                            <Text style={[styles.contetsHeading,{fontSize:14,marginTop:10}]}>{song?(song.name):(null)}</Text>
                                                            <Text style={[styles.contestBody,{fontSize:20}]}>${song?(song.price):(null)}</Text>
                                                            </View>
                                                                        
                                                        </View>
                                                    </View>,
                                                    {width: width/1.5, height: height/10, marginLeft: '5%', marginRight:'auto', borderRadius: 50,position:'absolute',top:height/20,opacity:0.96},10,'#303030'
                                                )}
                                            </View>              
                                         ):(null)}  
                                         {loader?(
                                             <ActivityIndicator size="large"  color="#fe4f5b" style={{marginTop:25}} />
                                         ):(null)} 
                                            <WebView
                                                // style={styles.container}
                                                originWhitelist={['*']}
                                                source={{uri:paymentUrl}}
                                                onMessage={(event)=>{
                                                    handleMessage(event.nativeEvent)
                                                }}
                                            />
                                        
                                    </View>
                                    </TouchableWithoutFeedback>,{width: width, height: height, marginLeft: 'auto', marginRight:'auto', borderRadius: 20,position:'absolute',top:height/10}
                                )}
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: 
    {
        flex: 1,
        flexDirection: 'column',
        width:width,
        height: height,
        margin:'auto', 
        // borderWidth: 1
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        // paddingTop:50,
    },
        header:
        {
            flexDirection: 'row',
            width: width,
            height: height*0.05,
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginLeft:10
        },
            postQueText:
            {
               fontSize: 24,
               marginLeft: '5%',
               marginTop: '2%',
               flexDirection: 'row',
               justifyContent: 'center'
            },
            closeIcon:
            {
                height: height*0.027,
                alignSelf: 'flex-end',
                width: width*0.06,
                marginLeft: 'auto',
                marginBottom: 10, 
                resizeMode: 'contain',
                position: 'relative',
                right:-width*0.07
            },
        queView:
        {
            marginTop: '1%',
            padding: 10
        },
            
        queDescView:
        {
            marginTop: -height*0.03,
            padding: 10,
            flexDirection:'row',
            justifyContent: 'center',
        },
            queDesc:
            {
                borderRadius: 10,
                padding: 4,
                // borderWidth: 0.5,
                backgroundColor:theme.inputBgColor
            },
        btnView: 
        {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 20,
            backgroundColor:theme.themeColor,
            width: width*0.3,
            marginBottom:10,
            marginLeft:-10
        },
                       
            btnText: 
            {
                fontSize: 18,
                fontWeight:'bold',
                color: theme.primaryColor,
                padding: 10,
            },
            contestBodyWrapper:{
                // width:170,
                // paddingHorizontal:16,
            },
            contetsHeading:{
                marginTop:4,
                color:'white',
                fontSize:18,
            },
            contestBody:{
                marginTop:4,
                color:'red',
                fontSize:20,
            },
    })