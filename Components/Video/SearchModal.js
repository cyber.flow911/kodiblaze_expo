import React, { Component ,useState,useEffect} from 'react';
import { View, Text,StyleSheet,ScrollView,TouchableOpacity,Dimensions,Image,TouchableWithoutFeedback, Modal, TextInput, ImageBackground,Linking,BackHandler,FlatList } from 'react-native';
import { Feather, Octicons,FontAwesome5 } from '@expo/vector-icons'
import {useDispatch,useSelector} from 'react-redux'
import CardView from '../Utils/CardView';
import {assets} from '../config'
import { SearchBar } from 'react-native-elements';
import {fetchUserSongs} from '../../Apis/Profile'
import {fetchSearchSongs} from '../../Apis/Songs'
import { Ionicons } from '@expo/vector-icons';


const width = Dimensions.get('window').width
const height = Dimensions.get('screen').height

export default function SearchModal(props) {

    const {visibleSearch , setVisibleSearch,songPlayed,setSongPlayed,userId} = props;
    const [search,setSearch] = useState('');
    const [songs , setSongs] = useState([]);

    useEffect(() => {
        fetchUserSongs(userId,songsCallback)
    },[]);

    function songsCallback(response) {
        // console.log("r",response)
        setSongs(response.data)
    }

    useEffect(() => {
        if(search == '')
            fetchUserSongs(userId,songsCallback)
        else
            fetchSearchSongs(userId,search,songsCallback)
    },[search])

    function setSong(item)
    {
        setSongPlayed(item);
    }
    const renderSongsList=(item)=>{
        return(
            <TouchableOpacity onPress={()=>setSong(item)}>
                    <View style={{display: 'flex', flex: 1,flexDirection: 'row',margin:10}}>
                    <Image source={assets.profile.songImage} style={{height:60,width:60}} />
                    <View style={[styles.contestBodyWrapper,{marginLeft:10}]}>
                        <Text style={[styles.contetsHeading,{fontSize:18}]}>{item.name}</Text>
                        <Text style={[styles.contestBody,{fontSize:12}]}>{item.downloads} Downloads  ${item.price}</Text>
                    </View>
                    {/* <TouchableOpacity style={{position: 'absolute',right:40}} 
                    onPress={() => {isPlaying&&playingSongId==item.id?(pauseSound()):(playSound(item))}} >
                        <Ionicons name={isPlaying&&playingSongId==item.id ? 'pause-outline' : 'play-outline'} size={30} color="white" />
                    </TouchableOpacity>    */}
                    <TouchableOpacity style={{position: 'absolute',right:0}} >
                        <Ionicons name={'cart-outline'} size={30} color="white" />    
                    </TouchableOpacity>          
                </View>
            </TouchableOpacity>
        )
    }
    return (
        <Modal
                animationType="slide"
                transparent={true}
                visible={visibleSearch}
                onRequestClose={()=>{
                    setVisibleSearch(false);
                  }}
            >
                    <TouchableWithoutFeedback onPress={()=> {}} >
                        {/* <ScrollView> */}
                            <View style={{width:'100%',height:'100%'}}>
                                <View>
                                    {CardView(
                                    <TouchableWithoutFeedback>
                                        <View style={styles.container}>
                                            <View style={styles.header}>
                                                <TouchableOpacity onPress={()=>{setVisibleSearch(false)}} style={{padding:2}}>
                                                    <FontAwesome5 name="arrow-left" size={20} color={'black'} style={{padding:2}} />
                                                </TouchableOpacity>
                                            </View>         
                                            <View style={{margin:10}}>
                                            <SearchBar
                                                    placeholder="Search Song"
                                                    onChangeText={(text)=>setSearch(text)}
                                                    value={search}
                                                />
                                            </View>
                                            <FlatList
                                                data={songs}
                                                renderItem={({item})=>renderSongsList(item)}
                                                keyExtractor={(item)=>item.id}
                                                style={{height:height/1.5}}
                                                // style={{height:150,width:'auto'}}
                                                
                                            /> 
                                        </View>
                                        </TouchableWithoutFeedback>,
                                        {width: width, height: height/1.5, marginLeft: 'auto', marginRight:'auto', borderRadius: 20,position:'absolute',top:height/2.5},10,'white'
                                    )}
                                </View>
                            </View>
                    
                        {/* </ScrollView>/ */}
                    </TouchableWithoutFeedback>
            </Modal>
    )

}


const styles = StyleSheet.create({
    container: 
    {
        flex: 1,
        flexDirection: 'column',
        width:width,
        height: height,
        margin:'auto', 
        // borderWidth: 1
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        // paddingTop:50,
    },
        header:
        {
            flexDirection: 'row',
            width: width,
            height: height*0.05,
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginLeft:10,
            padding:10
        },
            postQueText:
            {
               fontSize: 24,
               marginLeft: '5%',
               marginTop: '2%',
               flexDirection: 'row',
               justifyContent: 'center'
            },
            closeIcon:
            {
                height: height*0.027,
                alignSelf: 'flex-end',
                width: width*0.06,
                marginLeft: 'auto',
                marginBottom: 10, 
                resizeMode: 'contain',
                position: 'relative',
                right:-width*0.07
            },
        queView:
        {
            marginTop: '1%',
            padding: 10
        },
            
        queDescView:
        {
            marginTop: -height*0.03,
            padding: 10,
            flexDirection:'row',
            justifyContent: 'center',
        },
            queDesc:
            {
                borderRadius: 10,
                padding: 4,
                // borderWidth: 0.5,
                backgroundColor:'white'
            },
        btnView: 
        {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 20,
            backgroundColor:'black',
            width: width*0.3,
            marginBottom:10,
            marginLeft:-10
        },
                       
            btnText: 
            {
                fontSize: 18,
                fontWeight:'bold',
                color: 'black',
                padding: 10,
            },
            continue:{
                flexDirection:'row',
                justifyContent:'center',
                marginTop:20,
                marginBottom:20,
                alignItems: 'center',
                width:width,
                borderTopWidth:1,
                borderTopColor:'white',
                paddingTop:10,
                position:'absolute',
                bottom:height/15
        },
})