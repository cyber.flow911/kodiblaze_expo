import * as React   from 'react';
import { View, StyleSheet, TouchableOpacity,BackHandler,Dimensions,PermissionsAndroid,Alert,Platform } from 'react-native';
import { Video, AVPlaybackStatus,Audio } from 'expo-av';
import {useSelector,useDispatch} from 'react-redux'
import { Feather, Octicons,FontAwesome5 } from '@expo/vector-icons'
import * as MediaLibrary from 'expo-media-library';
import * as FileSystem from 'expo-file-system';
import * as Permissions from 'expo-permissions';
import {uploadVideoToServer} from '../../Apis/Video'
import LoaderModal from '../Utils/LoaderModal';
import SuccessModal from '../Utils/SuccessModal';
import {SET_VIDEO,SET_VIDEO_URL} from '../../Reducers/types';
import {songBaseUrl} from '../config'
import {useState,useEffect} from 'react'
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

export default function PlayVideo(props) {

  const video = React.useRef(null);
  const [status, setStatus] =useState({});
  const [hasPermission, setHasPermission] = useState(null);
  const [formData , setFormData] = useState(null);
  const [showModal , setModalVisible] = useState(false)
  const [showSuccessModal , setSuccessModalVisible] = useState(false);

  const platform = Platform.OS === 'ios'?('ios'):('android');

  const userDetails = useSelector((state)=>state.user.userDetails)
  const videoUrl = useSelector((state)=>state.user.videoUrl);
  const contestId = useSelector((state)=>state.user.contestId);
  const song = useSelector((state)=>state.user.song);
  // const recorded = props.route.params.recordedVideo?(true):(false)
  // const recordedVideoUrl = props.route.params.videoUrl

  const dispatch = useDispatch()
  const [sound,setSound] = React.useState();
  

   

  React.useEffect(() => {
    (async () => {
      if(PermissionsAndroid.RESULTS.GRANTED != 'granted')
      {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'My App Storage Permission',
            message: 'My App needs access to your storage ' +
              'so you can save your Videos',
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          },);
            setHasPermission(granted === 'granted')
            return granted;
      }
    })();
  }, []);

async function downloadVideo(){
    const filename="DownloadedVideo";
    const fileUri = `${FileSystem.documentDirectory}${filename}`;
    const downloadedFile = await FileSystem.downloadAsync(videoUrl, fileUri);

    if (downloadedFile.status != 200) {
      handleError();
    }


    const perm = await Permissions.askAsync(Permissions.MEDIA_LIBRARY);
    if (perm.status != 'granted') {
      return;
    }

    try {
      const asset = await MediaLibrary.createAssetAsync(downloadedFile.uri);
      const album = await MediaLibrary.getAlbumAsync('Download');
      if (album == null) {
        await MediaLibrary.createAlbumAsync('Download', asset, false);
      } else {
        await MediaLibrary.addAssetsToAlbumAsync([asset], album, false);
      }
    } catch (e) {
      handleError(e);
    }
 }

    BackHandler.addEventListener('hardwareBackPress', ()=>{handleBackPress()});

    function uploadVideo()
    {
      Alert.alert(
        "Upload Video?",
        "Do you want to upload video?",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => {
            setModalVisible(true);
            uploadVideoToServer(videoUrl,userDetails.name,userDetails.id,contestId,platform,song,uploadVideoCallback); 
          } }
        ]
      );
    }

    function uploadVideoCallback(response){
      if(response.msg == 'success')
      {
        setModalVisible(false);
        setSuccessModalVisible(true);
      }
      else
      {
        alert("Error occurred while uploading video!");
        setModalVisible(false);
      }
      video.current.pauseAsync()
    }

    function navigateToHome(){
      setSuccessModalVisible(false)
      dispatch({type:SET_VIDEO,payload:{recordVideo:false}})
      props.navigation.navigate("Home")
  }

    function handleBackPress() {
        video.current.pauseAsync()
        props.navigation.navigate('Record')
    }

    

    async function playSound() {

      // ('Loading Sound');
      const songUrl = songBaseUrl + song.song
      const { sound } = await Audio.Sound.createAsync(
              {uri:songUrl}
          );
      // setIsPlaying(true);
      setSound(sound);
      // ('Playing Sound');
      await sound.playAsync()
      video.current.playAsync()
  }

    useEffect(() => {
          return sound
          ? () => {
              // ('Unloading Sound');
              sound.unloadAsync(); }
          : undefined;
      }, [sound]);

  async function pauseSound() {
      const data = await sound.getStatusAsync()
      // ('Pausing Sound',data);
      await sound.pauseAsync(); 
      video.current.pauseAsync()
  }
    function playVideo(){
      if(platform == 'ios')
      {
        playSound()
      }
      else
      {
        video.current.playAsync()
      }
    }
    function pauseVideo(){
      if(platform == 'ios')
      {
        pauseSound();
      }
      else
      {
        video.current.pauseAsync()
      }
    }

  return (
    <View style={styles.container}>
         <TouchableOpacity onPress={()=>{handleBackPress()}} style={{position: 'absolute',top:60,left:10,zIndex:10}}>
                <FontAwesome5 name="arrow-left" size={20} color={'white'} style={{padding:2}} />
            </TouchableOpacity>
      <Video
        ref={video}
        style={styles.video}
        source={{
          uri: videoUrl.uri,
        }}
        useNativeControls={false}
        resizeMode="cover"
        isLooping
        onPlaybackStatusUpdate={status => setStatus(() => status)}
      >
      </Video>
      <LoaderModal showModal={showModal} setModalVisible={setModalVisible} displayTitle='This might take a while!'/>
      <SuccessModal showModal={showSuccessModal} setModalVisible={setSuccessModalVisible} displayTitle='The video is uploaded succesfully!' navigateToHome={navigateToHome}/>
      <View>
        {status.isPlaying?(
             <TouchableOpacity onPress={()=>{pauseVideo()}} style={{position: 'absolute',bottom:60,left:30,zIndex:10}}>
              <FontAwesome5 name="pause" size={24} color={'white'} style={{padding:2}}/>
            </TouchableOpacity>
        ):(
          <TouchableOpacity onPress={()=>{playVideo()}} style={{position: 'absolute',bottom:60,left:30,zIndex:10}}>
            <FontAwesome5 name="play" size={24} color={'white'} style={{padding:2}}/>
          </TouchableOpacity>
        )}
        <TouchableOpacity onPress={()=>{uploadVideo()}} style={{position: 'absolute',bottom:60,right:30,zIndex:10}}>
          <FontAwesome5 name="upload" size={24} color="white" />
        </TouchableOpacity>
      </View>
    </View>
    // onPress={() =>
    //   status.isPlaying ? video.current.pauseAsync() : video.current.playAsync()
    // }
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  video: {
    alignSelf: 'center',
    width: width,
    height: height,
    marginTop:height/21
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});