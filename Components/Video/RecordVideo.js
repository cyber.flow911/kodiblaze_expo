import React, { useRef,useState,useEffect } from 'react'
import { Text, StyleSheet, View, Image, ImageBackground, ScrollView,TouchableWithoutFeedback,TouchableOpacity,Dimensions ,ActivityIndicator,BackHandler,Modal} from 'react-native'
import {assets, theme,songBaseUrl} from '../config'
import { Feather, Octicons,FontAwesome5 } from '@expo/vector-icons'
import { Camera } from 'expo-camera';
import {SET_VIDEO,SET_VIDEO_URL,SET_SONG} from '../../Reducers/types';
import {useSelector,useDispatch} from 'react-redux'
import SearchModal from './SearchModal';
import {Audio,Video, AVPlaybackStatus} from 'expo-av'
import CardView from '../Utils/CardView';
// import { DeviceEventEmitter,NativeEventEmitter, NativeModules } from 'react-native';

const { height, width } = Dimensions.get('window');
const screenRatio = height / width;

export default function RecordVideo(props) 
{
    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [camRatios , setCameraRatio] = useState();
    const [isRatioSet, setIsRatioSet] = useState(false);
    const recordVideo = useSelector((state)=>state.user.recordVideo)
    const userDetails = useSelector((state)=>state.user.userDetails)
    const playVideoUrl = useSelector((state)=>state.user.videoUrl);
    const [search , setSearch] = useState('');
    const [visibleSearch , setVisibleSearch] = useState(false)
    const [songPlayed , setSongPlayed] = useState({})
    const [sound,setSound] = useState()
    const [isPlaying, setIsPlaying] = useState(false);
    const dispatch = useDispatch()
    const [isRecording, setIsRecording] = useState(false);
    const [videoRecordingLoader, setRecordingLoader ] = useState(false)
    const [videoUri,setVideoUri] = useState(null)
    const cameraRef = useRef();


    useEffect(() => {
    //   DeviceEventEmitter.addListener('Proximity', function (data) {
    //     // --- do something with events
    // });
    // const eventEmitter = new NativeEventEmitter(NativeModules.InCallManager);
    //   const eventListener = eventEmitter.addListener('Proximity', event => {
    //     // do something with events
    //   });
    },[])

    useEffect(() => {
      (async () => {
        // const { status } = await Camera.requestCameraPermissionsAsync();
        // // const Ratios = await Camera.getSupportedRatiosAsync();
        // await Audio.requestMicrophonePermissionsAsync();
        //     await Audio.setAudioModeAsync({
        //         allowsRecordingIOS: true,
        //         playsInSilentModeIOS: true,
        //     });
        // setHasPermission(status === 'granted');
        const { status } = await Camera.requestPermissionsAsync();
            await Audio.requestPermissionsAsync();
            await Audio.setAudioModeAsync({
                allowsRecordingIOS: true,
                playsInSilentModeIOS: true,
            });
            setHasPermission(status === 'granted');
        // setCameraRatio(Ratios);
      })();
    }, []);

  useEffect(() => {
    setVisibleSearch(false);
    setRecordingLoader(false);
    // console.log(songPlayed)
  },[songPlayed])

        async function playSound() {

          setVideoUri(null);
          setRecordingLoader(true);
          // console.log('Loading Sound');
          const songUrl = songBaseUrl + songPlayed.song
          const { sound } = await Audio.Sound.createAsync(
                  {uri:songUrl}
              );
          setIsPlaying(true);
          setSound(sound);
          // console.log('Playing Sound');
          takeVideo(sound);
      }

        useEffect(() => {
              return sound
              ? () => {
                  // console.log('Unloading Sound');
                  sound.unloadAsync(); }
              : undefined;
          }, [sound]);

      async function pauseSound() {
          const data = await sound.getStatusAsync()
          // console.log('Pausing Sound',data);
          setIsPlaying(false);
          await sound.pauseAsync(); 
      }

      const takeVideo = async (sound) => {
        if(Camera){
            await sound.playAsync(); 
            setRecordingLoader(false);
            setIsRecording(true);
            const data = await cameraRef.current.recordAsync({mirror:true,base64:true})
            console.log(data.uri);
            dispatch({type:SET_VIDEO_URL,payload:{videoUrl:data}})
            dispatch({type:SET_SONG,payload:{song:songPlayed}})
            // console.log('Recording')
            setIsRecording(false);
            setVideoUri(data.uri);

        }
      }

      const stopVideo = async () => {
        cameraRef.current.stopRecording();
        pauseSound();
      }

      useEffect(() => {
        if(playVideoUrl)
          props.navigation.navigate('PlayVideo')
      },[playVideoUrl])

    function startRecordingVideo()
    {
        if(songPlayed.name)
        {
            playSound()
        }
        else
        {
            setVisibleSearch(true)
        }
    }

    function stopVideoRecording()
    {
        stopVideo();
    }

  useEffect(() => {
    setSongPlayed({})
  },[props])

    
    const prepareRatio = async () => {
        let desiredRatio = '4:3';  // Start with the system default
        // This issue only affects Android
        if (Platform.OS === 'android') {
          const ratios = await Camera.getSupportedRatiosAsync();
    
          // Calculate the width/height of each of the supported camera ratios
          // These width/height are measured in landscape mode
          // find the ratio that is closest to the screen ratio without going over
          let distances = {};
          let realRatios = {};
          let minDistance = null;
          for (const ratio of ratios) {
            const parts = ratio.split(':');
            const realRatio = parseInt(parts[0]) / parseInt(parts[1]);
            realRatios[ratio] = realRatio;
            // ratio can't be taller than screen, so we don't want an abs()
            const distance = screenRatio - realRatio; 
            distances[ratio] = realRatio;
            if (minDistance == null) {
              minDistance = ratio;
            } else {
              if (distance >= 0 && distance < distances[minDistance]) {
                minDistance = ratio;
              }
            }
          }
          // set the best match
          desiredRatio = minDistance;
          //  calculate the difference between the camera width and the screen height
          const remainder = Math.floor(
            (height - realRatios[desiredRatio] * width) / 2
          );
          // set the preview padding and preview ratio
          setImagePadding(remainder / 2);
          setRatio(desiredRatio);
          // Set a flag so we don't do this 
          // calculation each time the screen refreshes
          setIsRatioSet(true);
        }
      };
    
    const setCameraReady = async() => {
        if (!isRatioSet) {
          await prepareRatio();
        }
      };
  
    if (hasPermission === null) {
      return <View />;
    }
    if (hasPermission === false) {
      return <Text>No access to camera</Text>;
    }
    BackHandler.addEventListener('hardwareBackPress', navigateToHome);
    function renderIcons(icon,func) {
        // console.log(func)
        return(
            <TouchableOpacity onPress={func} style={styles.button}>
                <Image source={icon} style={styles.icon}/>
            </TouchableOpacity>
        )
    }
    function flipCamera(){
        setType(
            type === Camera.Constants.Type.back
              ? Camera.Constants.Type.front
              : Camera.Constants.Type.back
          );
    }
    function navigateToHome(){
        dispatch({type:SET_VIDEO,payload:{recordVideo:false}})
        props.navigation.navigate("Home");
    }
    return (
      <View style={styles.container}>
        
          <>
                <Camera 
                  style={styles.camera} 
                  ratio="16:9"
                  type={type}
                  ref={cameraRef}
                  >
            <View style={styles.songContainer}>
                <View style={{display: 'flex',flexDirection:'column',justifyContent: 'flex-start'}}>
                    <Image source={assets.record.music} style={{height:height*0.025,width:width*0.06}} />
                </View>
                {isRecording?(null):(
                  <TouchableOpacity style={{position:'absolute',right:width*0.025}} onPress={()=>setVisibleSearch(!visibleSearch)}>
                    <Image source={assets.record.search} style={{height:height*0.028,width:width*0.06}} />
                </TouchableOpacity>
                )}
            </View>
            <View>
            
            </View>
            <View>
                <Text style={styles.songText}>{songPlayed.name?('Selected'):('Select Song')} <Text style={styles.songName}>{songPlayed.name}</Text></Text>
            </View>
            {isRecording?(
              null
            ):(
              <View style={styles.iconContainer}>
                  {/* <TouchableOpacity onPress={()=>props.navigation.navigate("Home")} style={styles.button}>
                      <Image source={assets.record.home} style={styles.icon}/>
                  </TouchableOpacity> */}
                  {/* {renderIcons(assets.record.like)} */}
                  {/* {renderIcons(assets.record.comments,()=>{openModal()})} */}
                  {renderIcons(assets.record.flipCamera,()=>flipCamera())}
                  {/* {renderIcons(assets.record.share)} */}
                  {renderIcons(assets.record.home,()=>navigateToHome())}
              </View>
            )}
          <View style={styles.buttonContainer}>
            {videoRecordingLoader?(
                         <ActivityIndicator size="large"  color="white" style={{marginTop:25}} />
            ):(
              
              isPlaying&&isRecording?(
                  <TouchableOpacity style={[styles.button,{borderWidth:5,borderColor:'red',borderRadius:100,width:width*0.2}]} onPress={()=>stopVideoRecording()}>
                    <Image source={assets.record.recordIcon} style={{height:height*0.08,width:width*0.14}} />
                  </TouchableOpacity>
              ):(
                <TouchableOpacity style={[styles.button]} onPress={()=>startRecordingVideo()}>
                  <Image source={assets.record.recordIcon} style={{height:height*0.1,width:width*0.17}} />
                </TouchableOpacity>
              )
            )}
            
          </View>
        </Camera>
                <SearchModal visibleSearch={visibleSearch} setVisibleSearch={setVisibleSearch} songPlayed={songPlayed} setSongPlayed={setSongPlayed}  userId={userDetails.id} />
          </>
        
      </View>
    );
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    camera: {
      flex: 1,
    },
    songContainer:{
        // flex: 1,
      backgroundColor: 'transparent',
      flexDirection: 'row',
      margin: 20,
      marginTop:70,
    },
    songText:{
        color:'white',
        fontSize:18,
        fontWeight: 'bold',
        marginLeft:20
    },
    songName:{
        color:'white',
        fontSize:15,
        fontWeight: '500',
    },
    header:
    {
        flexDirection: 'row',
        width: width,
        height: height*0.05,
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginLeft:10,
        padding:10
    },
    buttonContainer: {
      flex: 1,
      backgroundColor: 'transparent',
      flexDirection: 'row',
      margin: 20,
      justifyContent: 'center',
    },
    button: {
      // flex: 0.1,
      alignSelf: 'flex-end',
      alignItems: 'center',
    },
    iconContainer:{
        display: 'flex',
        flexDirection: 'column',
        flex:5,
        justifyContent: 'flex-end',
        marginTop:20
        // position: 'absolute',
        // bottom:height/7,
        // right:width/20,
    },
    icon:{
        height:height/30,
        width:width/12.5,
        margin:12,
        padding:4,
    },
    text: {
      fontSize: 18,
      color: 'white',
    },
  });
  