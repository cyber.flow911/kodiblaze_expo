import React, { useEffect,useState } from 'react'
import { Text, StyleSheet, View, ImageBackground,TouchableOpacity,TextInput, FlatList, ActivityIndicator,Dimensions } from 'react-native'
import {assets, theme,videoBaseUrl} from '../config'
import { Ionicons,FontAwesome5 } from '@expo/vector-icons';
import {fetchSingleVideo} from '../../Apis/Video'
import {Video} from 'expo-av'
import LoaderModal from '../Utils/LoaderModal'
import SuccessModal from '../Utils/SuccessModal'
import {commentOnVideo} from '../../Apis/Video'
import {useSelector,useDispatch} from 'react-redux'
import {SET_VIDEO,SET_USER_DETAILS} from '../../Reducers/types';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width

export default function VideoView(props){

    // console.log(props.route)
    const videoId = props.route.params.id;
    // console.log(comments)
    const userDetails = useSelector((state)=>state.user.userDetails)

    const [videoDetails , setVideoDetails] = useState(false)
    const [comments , setComments] = useState(false)
    const [comment , setComment] = useState('')
    const [loaderVisible,setLoaderVisible] = useState(false);
    const [successModal, setSuccessModal] = useState(false)
    const [pageLoader , setPageLoader] = useState(false);

    const dispatch = useDispatch()

    useEffect(() => {
        fetchSingleVideo(videoId,videoCallback);
        setPageLoader(true);
    },[videoId])

    // console.log(videoDetails)

    function videoCallback(response)
    {
        // console.log(response);
        if(response.msg == 'success')
        {
            setVideoDetails(response.video)
            setComments(response.comments);
            setPageLoader(false);
        }
    }
    function addComment()
    {
        if(comment == '')
        {
            alert("Please write a comment!");
        }
        else
        {
            setLoaderVisible(true);
            commentOnVideo(comment,userDetails.id,videoDetails.id,videoDetails.c_id,addCommentCallback);
        }
    }
    function addCommentCallback(response){
        // console.log(response);
        if(response.msg == 'success')
        {
            // setSuccessModal(true)
            setLoaderVisible(false);
            if(comment)
                var oldComment = comments;
            else
                var oldComment = [];
            oldComment.push({
                "c_id": videoDetails.c_id,
                "comments": comment,
                "email": userDetails.email,
                "id": null,
                "time_stamp": null,
                "user": userDetails.id,
                "userName": userDetails.name,
                "vid_id": videoId,
            })
            setComments(oldComment)
            setComment('');
        }
    }

    function playVideo()
    {
        dispatch({type:SET_VIDEO,payload:{recordVideo:true}})
        props.navigation.navigate('PlayRecordedVideo', { videoUrl:videoBaseUrl+videoDetails.video,recordedVideo:true,navigate:'Videos' })
    }


    const renderUserAndDetails=(item)=>{
        // console.log(item)
        return(
            <>
            <View style={styles.UserCommentWrapper}>
                <View>
                    <Ionicons name="person-circle-outline" size={30} color="white" />
                </View>
                <View>
                    <Text style={styles.TextWhite}> {item.userName} </Text>
                </View>
                <View>
                    {/* <Text style={[styles.TextWhite, styles.Font10, styles.italic]}>
                        few seconds ago
                    </Text> */}
                </View>
            </View>
            <Text style={[styles.TextWhite,{marginLeft:40}]}>
                {item.comments}
            </Text>
            </>
        )
    }


    function handleBackPress() {
        props.navigation.navigate('Videos')
    }

        return (
               pageLoader?(
                <View style={[styles.container,{marginTop:-50}]}>
                    <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
                        <ActivityIndicator size="large"  color="white" style={{marginTop:height/2.2}} />
                    </ImageBackground>
                </View>
               ):( <View styles={styles.container}>
                <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>
                <TouchableOpacity onPress={()=>{handleBackPress()}} style={{position: 'relative',top:60,left:10,zIndex:10,marginTop:-50,marginBottom:20}}>
                    <FontAwesome5 name="arrow-left" size={20} color={'white'} style={{padding:2}} />
                </TouchableOpacity>
                    <View style={styles.MainWrapper}>
                        {/* <Image source={assets.blog.blogImg} style={styles.VideoPoster} /> */}
                        <TouchableOpacity onPress={()=>{playVideo()}}>
                        <Video
                                style={styles.VideoPoster}
                                source={{
                                    uri: videoDetails?(videoBaseUrl+videoDetails.video):(null),
                                }}
                                useNativeControls={false}
                                resizeMode="cover"
                                
                                
                            />
                        </TouchableOpacity>
                        <View style={styles.VideoContentWrapper}>
                            <View>
                                <Text style={styles.videoTitle}>{videoDetails.name}</Text>
                                <Text style={styles.videoDescription}>{videoDetails.username}</Text>
                                {/* <Text style={[styles.videoDescription, styles.italic]}>2 days ago</Text> */}
                            </View>
                            <View>
                                <Ionicons name="ellipsis-vertical" size={24} color="white" />
                            </View>
                        </View>

                        <View style={styles.hr}></View>
                        <LoaderModal showModal={loaderVisible} setModalVisible={setLoaderVisible} />
                        <SuccessModal showModal={successModal} setModalVisible={setSuccessModal} displayTitle='Comment added successfully!' navigateToHome={()=>{setSuccessModal(false)}}/>
                    {/* <ScrollView> */}
                        
                        <Text style={styles.h3}>Comments</Text>

                        
                        <View>
                            {comments?(
                                <FlatList
                                    data={comments}
                                    renderItem={({item})=>renderUserAndDetails(item)}
                                    keyExtractor={(item)=>item.id}
                                />
                            ):(
                                <Text style={[styles.TextWhite,{margin:20}]}>No comments</Text>
                            )}
                        </View>
                    {/* </ScrollView> */}
                    <View style={styles.InputWrapper}>
                        <TextInput placeholder="Add Comment" value={comment} onChangeText={(text)=>{setComment(text)}} placeholderTextColor='black' style={styles.input}/>
                    </View>
                    <View style={{ flexDirection: 'row' ,justifyContent:'flex-end'}}>
                        <TouchableOpacity onPress={()=>{addComment()}} style={styles.addButton}>
                            <FontAwesome5 name="arrow-right" size={20} color={'white'} style={{padding:2}} />
                        </TouchableOpacity>
                    </View>
                    
                    </View>
                    </ImageBackground>
            </View>
        )
    )
}

const styles = StyleSheet.create({
    bgImage:{
        height:'100%',
        width:'100%',
    },
    container:{
        paddingHorizontal: 10,
        // flex: 1,
        
    },
    MainWrapper:{
        marginTop:50,
        paddingHorizontal:14,
    },
    VideoPoster:{
        height:130,
        width:'100%',
        borderRadius:18,
    },
    videoTitle:{
        color: theme.whiteColor,
        fontSize:18,
        fontWeight: 'bold',
    },
    videoDescription:{
        color: theme.whiteColor
    },
    italic:{
        fontStyle: 'italic',
        marginTop:4,
    },
    VideoContentWrapper:{
        marginTop:6,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    hr:{
        borderWidth:.5,
        borderColor: theme.whiteColor,
        marginTop:20,
    },
    h3:{
        fontSize:18,
        color: theme.whiteColor,
        marginTop:12,
        fontStyle: 'italic',
    },
    UserCommentWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:12,
    },
    TextWhite:{
        color: theme.whiteColor,
    },
    Font10:{
        fontSize:10
    },
    input:{
        padding:7.5,
        color:'black'
    },
    LinkAndCommentWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    mr2:{
        marginRight:10,
    },
    addButton:{
        position: 'relative',
        right:10,
        bottom:35,
        borderRadius:10,
        paddingHorizontal:10,
        borderTopRightRadius:50,
        borderBottomRightRadius:50,
        backgroundColor:'#ff6666',
        zIndex:10,
        padding:2
    },
    InputWrapper:{
        backgroundColor: theme.whiteColor,
        borderRadius: 40,
        marginTop:20,
        paddingHorizontal:12,
        paddingVertical:4,
    },
})
