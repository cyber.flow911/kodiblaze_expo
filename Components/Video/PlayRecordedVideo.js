import * as React from 'react';
import { View, StyleSheet, TouchableOpacity,BackHandler,Dimensions,PermissionsAndroid,Alert } from 'react-native';
import { Video, AVPlaybackStatus } from 'expo-av';
import {useSelector,useDispatch} from 'react-redux'
import { Feather, Octicons,FontAwesome5 } from '@expo/vector-icons'
import * as MediaLibrary from 'expo-media-library';
import * as FileSystem from 'expo-file-system';
import * as Permissions from 'expo-permissions';
import {uploadVideoToServer} from '../../Apis/Video'
import LoaderModal from '../Utils/LoaderModal';
import SuccessModal from '../Utils/SuccessModal';
import {SET_VIDEO,SET_VIDEO_URL} from '../../Reducers/types';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

export default function PlayVideo(props) {

 


  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});
  const [hasPermission, setHasPermission] = React.useState(null);
  const dispatch = useDispatch();
  const recorded = props.route.params.recordedVideo?(true):(false)
  const recordedVideoUrl = props.route.params.videoUrl
   

    BackHandler.addEventListener('hardwareBackPress', ()=>{handleBackPress()});

  
    function handleBackPress() {
        video.current.pauseAsync()
        dispatch({type:SET_VIDEO,payload:{recordVideo:false}})
        if(props.route.params.navigate == 'ContestPage')
            props.navigation.navigate(props.route.params.navigate,{contestId:props.route.params.contestId})
        else
            props.navigation.navigate(props.route.params.navigate)
    }

       //   function to pause song on navigation change
    // useEffect( () => {
    //     const unsubscribe = props.navigation.addListener('blur', async() => {
    //       video.current.playAsync();
    //     });
    //     return unsubscribe; // will be called when the component unmount occurs
    //   }, [props.navigation]);
    

  return (
    <View style={styles.container}>
         <TouchableOpacity onPress={()=>{handleBackPress()}} style={{position: 'absolute',top:60,left:10,zIndex:10}}>
                <FontAwesome5 name="arrow-left" size={20} color={'white'} style={{padding:2}} />
            </TouchableOpacity>
      <Video
        ref={video}
        style={styles.video}
        source={{
          uri: recordedVideoUrl,
        }}
        useNativeControls={false}
        resizeMode="cover"
        isLooping
        onPlaybackStatusUpdate={status => setStatus(() => status)}
      >
      </Video>
      <View>
        {status.isPlaying?(
             <TouchableOpacity onPress={()=>{video.current.pauseAsync()}} style={{position: 'absolute',bottom:60,left:30,zIndex:10}}>
              <FontAwesome5 name="pause" size={24} color={'white'} style={{padding:2}}/>
            </TouchableOpacity>
        ):(
          <TouchableOpacity onPress={()=>{video.current.playAsync()}} style={{position: 'absolute',bottom:60,left:30,zIndex:10}}>
            <FontAwesome5 name="play" size={24} color={'white'} style={{padding:2}}/>
          </TouchableOpacity>
        )}
      </View>
    </View>
    // onPress={() =>
    //   status.isPlaying ? video.current.pauseAsync() : video.current.playAsync()
    // }
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  video: {
    alignSelf: 'center',
    width: width,
    height: height,
    marginTop:height/21
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});