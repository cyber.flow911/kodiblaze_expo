import React, { useState,useEffect } from 'react'
import { Text, StyleSheet, View, Image,FlatList, ImageBackground, Dimensions,TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import {fetchAllVideos} from '../../Apis/Video'
import { Audio, Video } from 'expo-av';
import {assets, theme,imageBaseUrl,songBaseUrl,videoBaseUrl} from '../config'
import Shimmer from '../Utils/Shimmer'

const width = Dimensions.get('window').width

export default function Videos(props) {

    const [videos , setVideos] = useState([]);
    const [filter , setfilter] = useState('default');
    const [loader,setLoader] = useState(false);
    
    useEffect(() => {
        if(filter != '')
        {
            fetchAllVideos(filter,videosCallback)
            setLoader(true);
        }
    },[filter])

    function changeFilter(filter) {
        setfilter(filter);
        setTimeout(() =>{
            setfilter('')
        },2000)
    }

    function videosCallback(response) {
        console.log(response)
        if(response.msg === 'success')
        {
            setVideos(response.videos);
            setLoader(false);
        }
    }

    function renderVideoCard(item){
        return(
            <TouchableWithoutFeedback onPress={()=>props.navigation.navigate('VideoView',{id:item.id})}> 
                <View style={styles.videoCardWrapper}>
                    {/* <Image source={assets.blog.blogImg} style={styles.VideoPoster} /> */}
                    <Video
                         style={styles.VideoPoster}
                         source={{
                            uri: videoBaseUrl+item.video,
                         }}
                         useNativeControls={false}
                         resizeMode="cover"
                         
                    />
                    <View style={styles.VideoContentWrapper}>
                        <View>
                            <Text style={styles.videoTitle}>{item.name}</Text>
                            <Text style={styles.videoDescription}>{item.username}</Text>
                        </View>
                        <View>
                            <Ionicons name="ellipsis-vertical" size={24} color="white" />
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }


        return (
            <View styles={styles.container}>
            <ImageBackground source={assets.contest.contestBg} style={styles.bgImage}>

                <View style={styles.HeaderButtonWrapper}>
                    <TouchableOpacity onPress={()=>{changeFilter('latest')}}>
                        <Image source={assets.video.btnLatest} style={styles.headerBtn} />
                    </TouchableOpacity >
                    <TouchableOpacity onPress={()=>{changeFilter('random')}}>
                        <Image source={assets.video.colorBorder} style={styles.headerBtn} />
                        <View style={{backgroundColor:'black'}}>
                            <Text style={[styles.videoDescription,{fontSize:12,zIndex:10,position: 'relative',bottom:20,left:17}]}>Random</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{changeFilter('oldest')}}>
                        <Image source={assets.video.colorBorder} style={styles.headerBtn} />
                        <View style={{backgroundColor:'black'}}>
                            <Text style={[styles.videoDescription,{fontSize:13,zIndex:10,position: 'relative',bottom:20,left:20}]}>Oldest</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                {loader?(
                    <View style={{marginLeft:20}}>
                        <Shimmer width={width-30} height={120} />
                    </View>
                ):(
                    <FlatList
                        data={videos}
                        renderItem={({item})=>renderVideoCard(item)}
                        keyExtractor={(item)=>item.id}
                    />
                )}
            
            </ImageBackground>
            </View>
        )
    }

const styles = StyleSheet.create({
    bgImage:{
        height:'100%',
        width:'100%',
    },
    container:{
        paddingHorizontal: 10,
        flex: 1,
    },
    HeaderButtonWrapper:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal:30,
        marginTop:50,
        marginBottom:30,
    },
    headerBtn:{
        width:80.1,
        height:24
    },
    headerBtnRecomended:{
        height:30,
        width:95.8,
    },
    videoCardWrapper:{
        marginBottom:40,
        paddingHorizontal:20,
    },
    VideoPoster:{
        height:130,
        width:'100%',
        borderRadius:18,
    },
    videoTitle:{
        color: theme.whiteColor,
        fontSize:18,
        fontWeight: 'bold',
    },
    videoDescription:{
        color: theme.whiteColor
    },
    VideoContentWrapper:{
        marginTop:6,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
})
