import React, { Component,useState } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import LogIn from './LogIn'
import SignIn from './SignIn'

export default function Auth(props) {
    const [authMode ,setAuthMode] = useState(0);
        if(authMode == 1)
        {
            return (
                <View>
                    <LogIn setAuthMode={setAuthMode} navigation={props.navigation}/>
                </View>
            )
        }
        else
        {
            return (
                <View>
                    <SignIn setAuthMode={setAuthMode} navigation={props.navigation}/>
                </View>
            )
        }
}

const styles = StyleSheet.create({})
