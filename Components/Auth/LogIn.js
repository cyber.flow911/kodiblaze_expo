import React, { Component,useState } from 'react'
import { Text, StyleSheet, View, TextInput, Image, ImageBackground,Platform,TouchableWithoutFeedback,TouchableOpacity,ScrollView ,Dimensions} from 'react-native'
import {theme, assets,socialAuth} from '../config'
import {useDispatch} from 'react-redux';
import {SET_AUTH_STATUS,SET_USER_DETAILS} from '../../Reducers/types';
import {loginUser,googleLogin} from '../../Apis/Auth'
import LoaderModal from '../Utils/LoaderModal'
import * as Google from 'expo-google-app-auth';
import AsyncStorage from '@react-native-async-storage/async-storage';


const height = Dimensions.get('window').height

export default function LogIn(props){
 
    const {setAuthMode} = props;
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('');
    const [loaderModal , setLoaderModal] = useState(false);
    const [error , setError] = useState('');
    const [showError,setShowError] = useState(false);
    const dispatch = useDispatch();
    function login(){
        setShowError(false)
        setLoaderModal(true);
        if(email == '' || password == '')
        {
            setLoaderModal(false);
            setError('Please fill the fields!');
            setShowError(true);
        }
        else
        {
            loginUser(email, password,loginCallback)
        }
        
        // AsyncStorage.setItem('authStatus', true)  
    }
    function loginCallback(response) {
        if(response.msg == 'success')
        {
            dispatch({type:SET_AUTH_STATUS,payload:{authStatus:true}})
            dispatch({type:SET_USER_DETAILS,payload:{userDetails:response.data}})
            setLoaderModal(false);
            AsyncStorage.setItem('user_details', JSON.stringify(response.data))  ;
            AsyncStorage.setItem('authStatus', 'true')  ;
            props.navigation.navigate('Home');
        }
        else
        {
            setError('Invalid Credentials!');
            setShowError(true); 
            setLoaderModal(false);
        }
    }

    async function signInWithGoogleAsync() {
        setLoaderModal(true);
        try {
          const result = await Google.logInAsync({
            androidClientId: socialAuth.google.androidClientId,
            iosClientId: socialAuth.google.iosClientId,
            iosStandaloneAppClientId: socialAuth.google.iosStandaloneAppClientId,
            androidStandaloneAppClientId: socialAuth.google.androidStandaloneAppClientId,
            scopes: ['profile', 'email'],
          });
      
          if (result.type === 'success') {
            //   console.log(result.user)
              googleLogin(result.user.email,'google',loginCallback);
            return result.accessToken;
          } else {
            setLoaderModal(false);
            return { cancelled: true };
          }
        } catch (e) {
            setLoaderModal(false);
          return { error: true };
        }
      }
    
        return (
            <>
                <ScrollView>
                <View style={styles.container}>
                <ImageBackground source={assets.auth.bg} style={styles.backgroundImg}>

                
                <View style={styles.formWrapper}>
                    {showError?(
                    <View style={styles.singUpWrapper}>
                        <Text style={[styles.singUpText,{fontSize:15,color:'red',marginTop:-30,marginBottom:10}]}>{error}</Text>
                    </View>):(null)}
                    
                        <View style={styles.InputWrapper}>
                            <TextInput placeholder="Email" onChangeText={(text)=>{setEmail(text)}} placeholderTextColor='black' style={styles.input}/>
                        </View>
                        <View style={styles.InputWrapper}>
                            <TextInput secureTextEntry={true} onChangeText={(text)=>{setPassword(text)}} placeholder="Password" placeholderTextColor='black' style={styles.input}/>
                        </View>
                    </View>

                    <View>
                        <TouchableOpacity onPress={() =>login()}>
                            <Image source={assets.auth.btnSignIn} style={styles.btnSignIn} />
                        </TouchableOpacity>
                    </View>
                    <LoaderModal showModal={loaderModal} setModalVisible={setLoaderModal}/>
                    <View style={styles.singUpWrapper}>
                        <Text style={styles.singUpText}>Sign up using</Text>
                        <View style={styles.IconWrapper}>
                            <TouchableOpacity onPress={()=>{signInWithGoogleAsync()}}>
                                <Image source={assets.auth.googleLogo} style={styles.Icon} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.bottomTextWrapper}>
                        <Text style={styles.alreadyText}>
                            Create a new account? 
                        </Text>
                        <TouchableWithoutFeedback onPress={()=>setAuthMode(0)}>
                            <Text style={styles.logInLink}> Sign up</Text>
                        </TouchableWithoutFeedback>
                    </View>
                </ImageBackground>
                </View>
                </ScrollView>
            </>
        )
}

const styles = StyleSheet.create({
    container:{
        paddingTop:Platform.OS == 'ios' ? 20 : 45,
        backgroundColor: theme.blackColor
    },
    backgroundImg:{
        width:'100%',
        height:height,
    },
    input:{
        padding:7.5,
        color:'black'
    },
    formWrapper:{
        marginTop:350,
        paddingHorizontal:30,
    },
        InputWrapper:{
            backgroundColor: theme.whiteColor,
            borderRadius: 40,
            marginBottom:20,
            paddingHorizontal:12,
            paddingVertical:4,
        },
    btnSignIn:{
        alignSelf: 'center',
        width:120,
        height:41,
        marginBottom:30,
        marginTop:20
    },
    singUpWrapper:{
        alignSelf: 'center',
    },
    singUpText:{
        color: theme.whiteColor,
        fontSize:26,
    },
    IconWrapper:{
        flexDirection: 'row',
        justifyContent: 'center'
    },
        Icon:{
            height:40,
            width:40,
            marginHorizontal:12,
            marginTop:10,
            
        },
    bottomTextWrapper:{
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop:20,
    },
    alreadyText:{
        color: theme.whiteColor,
        fontSize:20
    },
    logInLink:{
        color:theme.linkColor,
        fontSize:20
    }
})
