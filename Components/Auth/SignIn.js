import React, { useState } from 'react'
import { Text, StyleSheet, View, TextInput, Image, ImageBackground, TouchableWithoutFeedback, TouchableOpacity,ScrollView,Dimensions } from 'react-native'
import {theme, assets,socialAuth} from '../config'
import {SET_AUTH_STATUS,SET_USER_DETAILS} from '../../Reducers/types';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import LoaderModal from '../Utils/LoaderModal'
import {emailCheck,signUpUser,googleSignUp,googleLogin} from '../../Apis/Auth';
import * as Google from 'expo-google-app-auth';

const height = Dimensions.get('window').height

export default function SignIn (props) {
    const {setAuthMode} = props;
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('');
    const [fName,setfName] = useState('');
    const [lName,setlName] = useState('');
    const [loaderModal , setLoaderModal] = useState(false);
    const [error , setError] = useState('');
    const [showError,setShowError] = useState(false);
    const [googleLoginTrue , setGoogleLogin] = useState(false);
    const [username , setUsername ] = useState('');
    const [profilePic , setProfilePic] = useState('')
    const dispatch = useDispatch()
    function signUp(){
        setShowError(false)
        setGoogleLogin(false);
        setLoaderModal(true);
        if(email == '' || password == '' || fName == '' || lName == '')
        {
            setLoaderModal(false);
            setError('Please fill the fields!');
            setShowError(true);
        }
        else
        {
            emailCheck(email, emailCallback)
        }
        // AsyncStorage.setItem('authStatus', true)  
    }
    function emailCallback(response,email,name,photoUrl)
    {
        if(response.msg == 'exists')
        {
            if(name != '' || name!=null)
            {
                googleLogin(email,'google',signUpCallback);
                return ;
            }
            setLoaderModal(false);
            setError('Email Id already Exists!');
            setShowError(true);
        }
        else if(response.msg == 'success')
        {
            name!=''||name!=null?(
                googleSignUp(email,name,photoUrl,signUpCallback)
            ):(
                signUpUser(email,password,fName,lName,signUpCallback)
            )
        }
    }
    function signUpCallback(response)
    {
        if(response.msg == 'success')
        {
            dispatch({type:SET_AUTH_STATUS,payload:{authStatus:true}})
            dispatch({type:SET_USER_DETAILS,payload:{userDetails:response.data}});
            AsyncStorage.setItem('user_details', JSON.stringify(response.data))  ;
            AsyncStorage.setItem('authStatus', 'true')  ;
            props.navigation.navigate('Home');
        }
        setLoaderModal(false);
    }

    async function signInWithGoogleAsync() {
        setLoaderModal(true);
        setGoogleLogin(true);
        try {
          const result = await Google.logInAsync({
            androidClientId: socialAuth.google.androidClientId,
            iosClientId: socialAuth.google.iosClientId,
            iosStandaloneAppClientId: socialAuth.google.iosStandaloneAppClientId,
            androidStandaloneAppClientId: socialAuth.google.androidStandaloneAppClientId,
            scopes: ['profile', 'email'],
          });
      
          if (result.type === 'success') {
              emailCheck(result.user.email, (response)=>emailCallback(response,result.user.email,result.user.name,result.user.photoUrl))
            return result.accessToken;
          } else {
            setLoaderModal(false);
            return { cancelled: true };
          }
        } catch (e) {
            setLoaderModal(false);
          return { error: true };
        }
      }
        return (
            <ScrollView>
            <View style={styles.container}>
               <ImageBackground source={assets.auth.bg} style={styles.backgroundImg}>

                <View style={styles.formWrapper}>

                    {showError?(
                        <View style={styles.singUpWrapper}>
                            <Text style={[styles.singUpText,{fontSize:15,color:'red',marginTop:5,marginBottom:10}]}>{error}</Text>
                        </View>):(null)}
                        <View style={styles.InputWrapper}>
                            <TextInput placeholder="First Name" placeholderTextColor='black' onChangeText={(text)=>{setfName(text)}} style={styles.input}/>
                        </View>
                        <View style={styles.InputWrapper}>
                            <TextInput placeholder="Last Name" placeholderTextColor='black' onChangeText={(text)=>{setlName(text)}} style={styles.input}/>
                        </View>
                        <View style={styles.InputWrapper}>
                            <TextInput placeholder="Email" placeholderTextColor='black' onChangeText={(text)=>{setEmail(text)}} style={styles.input}/>
                        </View>
                        <View style={styles.InputWrapper}>
                            <TextInput secureTextEntry={true} placeholder="Password" placeholderTextColor='black' onChangeText={(text)=>{setPassword(text)}} style={styles.input}/>
                        </View>
                    </View>
                    <LoaderModal showModal={loaderModal} setModalVisible={setLoaderModal}/>
                    <View>
                        <TouchableOpacity onPress={()=>signUp()}>
                            <Image source={assets.auth.btnSignIn} style={styles.btnSignIn} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.singUpWrapper}>
                        <Text style={styles.singUpText}>Sign up using</Text>
                        <View style={styles.IconWrapper}>
                            <TouchableOpacity onPress={()=>signInWithGoogleAsync()}>
                                <Image source={assets.auth.googleLogo} style={styles.Icon} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.bottomTextWrapper}>
                        <Text style={styles.alreadyText}>
                            Already have an account? 
                        </Text>
                        <TouchableWithoutFeedback onPress={()=>setAuthMode(1)}>
                            <Text style={styles.logInLink}> Log in</Text>
                        </TouchableWithoutFeedback>
                    </View>

               </ImageBackground>
            </View>
            </ScrollView>

        )
}

const styles = StyleSheet.create({
    container:{
        paddingTop:Platform.OS == 'ios' ? 20 : 40,
        backgroundColor: theme.blackColor,
    },
    input:{
        padding:7.5,
        color:'black'
    },
    backgroundImg:{
        width:'100%',
        height:height,
    },
    formWrapper:{
        marginTop:height/2.5,
        paddingHorizontal:30,
    },
        InputWrapper:{
            backgroundColor: theme.whiteColor,
            borderRadius: 40,
            marginBottom:20,
            paddingHorizontal:12,
            paddingVertical:4,
        },
    btnSignIn:{
        alignSelf: 'center',
        width:120,
        height:41,
        marginBottom:20
    },
    singUpWrapper:{
        alignSelf: 'center',
    },
    singUpText:{
        color: theme.whiteColor,
        fontSize:26,
    },
    IconWrapper:{
        flexDirection: 'row',
        justifyContent: 'center'
    },
        Icon:{
            height:40,
            width:40,
            marginHorizontal:12,
            marginTop:10,
            
        },
    bottomTextWrapper:{
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop:10,
    },
    alreadyText:{
        color: theme.whiteColor,
        fontSize:20
    },
    logInLink:{
        color:theme.linkColor,
        fontSize:20
    }
})
