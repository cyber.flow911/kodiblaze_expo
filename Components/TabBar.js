import React, { useEffect,useState } from 'react'
import { StyleSheet,View,Text,TouchableOpacity,Image, TouchableWithoutFeedback,Dimensions } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons'; 
import {assets} from './config'
import {SET_VIDEO,SET_PROPS} from '../Reducers/types';
import {useSelector,useDispatch} from 'react-redux'
import SelectContest from '../Components/Home/SelectContest'
const height = Dimensions.get('window').height;
export default function TabBar(props) {
  
    const {authStatus} = props;
    const [videoScreen , setVideoScreen] = useState(false);
    const recordVideo = useSelector((state)=>state.user.recordVideo)
    const dispatch = useDispatch()
    const [modalVisible,setModalVisible] = useState(false);

    useEffect(() => {
        dispatch({type:SET_PROPS,payload:{props:props}})
    },[props]);
    //creating single generic  tab item structure
    const renderTabItem =  (icon , label ,fun) =>
    {
        return(
                <TouchableWithoutFeedback onPress={fun}>
                        <View style={{justifyContent: 'space-between',margin:10,alignItems: 'center'}}>
                            <AntDesign name={icon} size={24} color="white" />
                            <Text style={{color:'white',fontSize:10}}>{label}</Text>
                        </View>
                </TouchableWithoutFeedback>
        )
    }
    function enableVideoScreen(){
        dispatch({type:SET_VIDEO,payload:{recordVideo:true}})
        props.navigation.navigate("Record");
        setModalVisible(false)
    }
    function navigateToScreen(screenName)
    {
        setVideoScreen(false);
        dispatch({type:SET_VIDEO,payload:{recordVideo:false}})
        props.navigation.navigate(screenName)
    }
    return (
        authStatus?(
            recordVideo?(null):(
                <>
                <SelectContest modalVisible={modalVisible} setModalVisible={setModalVisible} navigation={props.navigation}/>
                <View style={[{display:'flex',flexDirection: 'row',justifyContent: 'space-between',backgroundColor:'black'}]}>
                    <View>
                        {renderTabItem("home","Home",()=>navigateToScreen("Home"))}
                    </View>
                    <View>
                        {renderTabItem("menufold","Contest",()=>navigateToScreen("Contest"))}
                    </View>
                    <View>
                        <TouchableWithoutFeedback onPress={()=>setModalVisible(true)}>
                        {/* <TouchableWithoutFeedback onPress={()=>enableVideoScreen()}> */}
                            <Image source={assets.record.recordIcon} style={{height:40,width:40}}/>
                        </TouchableWithoutFeedback>
                    </View>
                    <View>
                        {renderTabItem("message1","Blogs",()=>navigateToScreen("Blogs"))}
                    </View>
                    <View>
                        {renderTabItem("videocamera","Video",()=>navigateToScreen("Videos"))}
                    </View>
                    {/* <View>
                        {renderTabItem("user","Profile",()=>props.navigation.navigate("Profile"))}
                    </View> */}
                </View>
                </>
            )
        ):(null)
    )
}


const styles = StyleSheet.create({})
