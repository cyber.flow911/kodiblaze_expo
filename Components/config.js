 export const theme = {
     blackColor: "#1b191e",
     whiteColor: "#ffffff",
     linkColor: '#08bfc2'
 }
export const socialAuth = {
    google:{
        androidClientId: '625844667438-7sskga75i48uuqc2pqrbinnk42invppm.apps.googleusercontent.com',
        webClientId: '625844667438-7sskga75i48uuqc2pqrbinnk42invppm.apps.googleusercontent.com',
        iosClientId: '625844667438-jmuf4ac5e7qpi6eh28d2kmdk1bo9j90h.apps.googleusercontent.com',
        iosStandaloneAppClientId:"625844667438-d5bcnp850m1qn3vmh84r24a8ku7maa61.apps.googleusercontent.com",
        androidStandaloneAppClientId:"625844667438-5rp3mnmtf0hgf1guef10kr43eij7r24l.apps.googleusercontent.com"
    },
}

export const serverBaseUrl = "https://kodiblaze.com/";
// export const serverBaseUrl = "http://192.168.95.184/voting/"
//server domain url will used as base for apis connections and  image full path resolution
export const imageBaseUrl = serverBaseUrl 
export const songBaseUrl = serverBaseUrl + "admin"
export const videoBaseUrl = serverBaseUrl 
export const serverApiUrl = serverBaseUrl +"AppApis/"; //server api url will used as base for apis connections

    export const assets = {
       
        auth:{
            bg:require("../assets/authBanner.png"),
            btnSignIn:require('../assets/btnSignIn.png'),
            googleLogo:require('../assets/googleLogo.png'),
            facebookLogo:require('../assets/facebookLogo.png'),
        },
        home:{
            KodiblazeBannerImg:require('../assets/KodiblazeBannerImg.png'),
            backIcon:require('../assets/backIcon.png'),
            profileIcon:require('../assets/profileIcon.png'),
        },
        contest:{
            contestBg:require('../assets/contestBg.png'),
            btnOngoing:require('../assets/btnOngoing.png'),
            btnUpcoming:require('../assets/btnUpcoming.png'),
            WallPrimaryWrapper:require('../assets/WallPrimaryWrapper.png'),
            WallCard: require('../assets/WallCard.png'),
            contestImg:require('../assets/contestImg.png'),
        },
        blog:{
            blogImg:require('../assets/blogImg.png'),
        },
        video:{
            btnLatest:require('../assets/btnLatest.png'),
            btnRecommended:require('../assets/btnRecommended.png'),
            btnMixes:require('../assets/btnMixes.png'),
            colorBorder:require('../assets/colorBorder.png'),
        },
        record:{
            recordIcon : require('../assets/record.png'), 
            music:require('../assets/music.png'),
            search:require('../assets/search.png'),
            comments:require('../assets/comment.png'),
            share: require('../assets/share.png'),
            home:require('../assets/home.png'),
            like:require('../assets/like.png'),
            unlike:require('../assets/unlike.png'),
            commentIcon:require('../assets/commentIcon.png'),
            flipCamera:require('../assets/flipCamera.png'),
        },
        profile:{
            profileImage: require('../assets/profile.png'),
            video: require('../assets/video.png'),
            editIcon:require('../assets/editIcon.png'),
            contestImage:require('../assets/contestImage.png'),
            songImage:require('../assets/songImage.png'),
            borderBg :require('../assets/borderBg.png'),
        },
        leaderboard:{
            firstRank:require('../assets/1st.png'),
            secondRank:require('../assets/2nd.png'),
            thirdRank:require('../assets/3rd.png'),
            leaderBoardBg:require('../assets/leaderBoardBg.png')
        },
        song:{
            blurImage : require('../assets/blurBg.jpeg')
        }
        
    }