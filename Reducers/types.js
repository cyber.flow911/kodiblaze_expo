/* 
        define and export all the action types here and use them in actions as well as in reducers

        ==> export const ACTION_TYPE = "ACTION_TYPE"


*/

export const SET_USER_DETAILS= "SET_USER_DETAILS";
export const SET_AUTH_STATUS= "SET_AUTH_STATUS";
export const SET_VIDEO = "SET_VIDEO";
export const SET_PROPS = "SET_PROPS";
export const SET_VIDEO_URL="SET_VIDEO_URL";
export const SET_CONTEST_ID="SET_CONTEST_ID";
export const SET_SONG="SET_SONG";