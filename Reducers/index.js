import * as actionTypes from "./types";
import { combineReducers } from "redux";
const initial_user_state={
    authStatus:false,
    userDetails:{
    },
    recordVideo:false,
    props:{},
    videoUrl:false,
    song:{},
}

const user_reducer=(state=initial_user_state,action)=>
{
    switch (action.type) {
        case actionTypes.SET_AUTH_STATUS:
            return{
                ...state,
                authStatus: action.payload.authStatus,
            }
        case actionTypes.SET_USER_DETAILS:
            return{
                ...state,
                userDetails: action.payload.userDetails,
            }
        case actionTypes.SET_VIDEO:
            return{
                ...state,
                recordVideo : action.payload.recordVideo,
            }
        case actionTypes.SET_PROPS:
            return{
                ...state,
                props : action.payload.props,
            }
        case actionTypes.SET_VIDEO_URL:
            return{
                ...state,
                videoUrl: action.payload.videoUrl,
            }
        case actionTypes.SET_CONTEST_ID:
            return{
                ...state,
                contestId: action.payload.contestId,
            }
        case actionTypes.SET_SONG:
            return{
                ...state,
                song : action.payload.song,
            }
        default:return state
    }
}



const rootReducer = combineReducers({
     user:user_reducer
})


export default rootReducer