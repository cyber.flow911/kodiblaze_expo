import { serverApiUrl, serverBaseUrl } from "../Components/config";
import mime from "mime";

//Api to upload userImage to server     
export const uploadVideoToServer=(uri,name,userId,contestId,platform,song,callback)=>
{
    var formData   = new FormData();
    formData.append("uploadVideo",true)
    formData.append("contestId",contestId)
    formData.append("userId",userId)
    formData.append("platform",platform); // will be merged if platform is ios
    formData.append("song",song.song)//song is an object having path as song.song
    formData.append("video[]",{ 
        uri : uri.uri.toString(),
        type: mime.getType(uri.uri),
        name: name.replace(/\s+/g, '')+userId+'_'+contestId+'.mp4',
    })
   
    // formData.append("video",{uri:uri})
    fetch(serverApiUrl+'video.php',
    {
        method: 'POST', 
        body:  formData,
       
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        }, 
    }).then((response)=>response.json())
    .then((response)=>{
        callback(response)
        // return response.msg;
    })
    .catch((error) => {
        console.error(error);
    });
}

export function fetchAllVideos(filter,callback) {
    let bannerData = new FormData();
    bannerData.append('fetchAllVideos',true);
    bannerData.append('filter',filter);
    fetch(serverApiUrl+'video.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}

export function fetchSingleVideo(videoId, callback) {

    let bannerData = new FormData();
    bannerData.append('fetchSingleVideo',true);
    bannerData.append('videoId',videoId)
    fetch(serverApiUrl+'video.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}

export const commentOnVideo=(comment,userId,videoId,contestId,callback)=>{
    let formData = new FormData();
    formData.append("addComment");
    formData.append("userId", userId);
    formData.append("videoId", videoId);
    formData.append("contestId", contestId);
    formData.append("comment", comment);
    fetch(serverApiUrl+'video.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: formData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}