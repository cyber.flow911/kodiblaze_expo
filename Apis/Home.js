import { serverApiUrl, serverBaseUrl } from "../Components/config";

//Api to fetch Home Banner
export const fetchBanner=(callback)=>{
    let bannerData = new FormData();
    bannerData.append('fetchBanner',true);
    fetch(serverApiUrl+'home.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json',
         
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
        // console.log(result)
    })
    .catch((error) => {
        console.error(error);
    });

}

//Api to fetch latest video
export const fetchVideo=(callback)=>{
    let videoData = new FormData();
    videoData.append('fetchLatestVideos',true);
    fetch(serverApiUrl+'home.php', {
        method: 'POST',
        headers: {
        Accept: 'application/json',
        'Content-Type':'application/json' 
        },  
        body: videoData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}

//Api to fetch Latest songs 
export const fetchSongs=(callback)=>{
    let songsData = new FormData();
    songsData.append('fetchLatestSongs',true);
    fetch(serverApiUrl+'home.php', {
        method: 'POST',
        headers: {
        Accept: 'application/json',
        'Content-Type':'application/json' 
        },  
        body: songsData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}

//Api to fetch latest blogs
export const fetchBlogs=(callback)=>{
    let blogsData = new FormData();
    blogsData.append('fetchLatestBlogs',true);
    fetch(serverApiUrl+'home.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: blogsData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}