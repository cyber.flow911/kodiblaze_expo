import { serverApiUrl, serverBaseUrl } from "../Components/config";

//Api to login User
export const loginUser=(email,password,callback)=>{
    let loginData = new FormData();
    loginData.append('Login',true);
    loginData.append('email',email);
    loginData.append('password',password);
    fetch(serverApiUrl+'auth.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: loginData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
    

}

   //api to login google user
   export const googleLogin=(email,type,callback)=>{
        let loginData = new FormData();
        loginData.append('googleLogin',true);
        loginData.append('email',email);
        loginData.append('type',type);
        fetch(serverApiUrl+'auth.php', {
            method: 'POST',
            // headers: {
            // Accept: 'application/json',
            // 'Content-Type':'application/json' 
            // },  
            body: loginData
        })
        .then((response) => response.json())
        .then(result=>{  
            callback(result)
        })
        .catch((error) => {
            console.error(error);
        });
    
 
   }

   //Api for checking user Email
   export const emailCheck=(email,callback)=>{
    let emailData = new FormData();
    emailData.append('checkEmail',true);
    emailData.append('email',email)
    fetch(serverApiUrl+'auth.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: emailData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
    
 
   }

   //Api for sign up User
   export const signUpUser=(email,password,fName,lName,callback)=>{
    let signUpData = new FormData();
    signUpData.append('signIn',true)
    signUpData.append('email',email);
    signUpData.append('password',password);
    signUpData.append('fName',fName);
    signUpData.append('lName',lName);
    fetch(serverApiUrl+'auth.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: signUpData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
    
 
   }

   export const googleSignUp=(email,username,profilePic,callback)=>{
        let signUpData = new FormData();
        signUpData.append('googleSignIn',true)
        signUpData.append('email',email);
        signUpData.append('username',username);
        signUpData.append('profilePic',profilePic);
        fetch(serverApiUrl+'auth.php', {
            method: 'POST',
            // headers: {
            // Accept: 'application/json',
            // 'Content-Type':'application/json' 
            // },  
            body: signUpData
        })
        .then((response) => response.json())
        .then(result=>{  
            callback(result)
        })
        .catch((error) => {
            console.error(error);
        });
        
   }
