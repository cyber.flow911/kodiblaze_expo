import { serverApiUrl, serverBaseUrl } from "../Components/config";

//Api to fetch user songs
export const fetchUserSongs=(userId,callback)=>{
    let songsData = new FormData();
    songsData.append('purchasedSongs',true);
    songsData.append('userId',userId);
    fetch(serverApiUrl+'profile.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: songsData,
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}

//Api to fetch user details
export const fetchUserDetails=(userId,callback)=>{
    let userData = new FormData();
    userData.append('getProfileDetails',true);
    userData.append('userId',userId);
    fetch(serverApiUrl+'profile.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: userData,
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}

//Api to fetch user contests
export const fetchUserContests=(userId,callback)=>{
    let userData = new FormData();
    userData.append('userContests',true);
    userData.append('userId',userId);
    fetch(serverApiUrl+'profile.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: userData,
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}

//Api to fetch user videos 
export const fetchUserVideos=(userId,callback)=>{
    let userData = new FormData();
    userData.append('userVideos',true);
    userData.append('userId',userId);
    fetch(serverApiUrl+'profile.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: userData,
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}

//Api to upload userImage to server     
export const uploadImageToServer=(uri,name,type,userId,callback)=>
{
    var formData   = new FormData();
    formData.append("uploadPhoto",true)
    formData.append("userId",userId)
    formData.append("images",{uri:uri,name:name,type})
    fetch(serverApiUrl+'profile.php',
    {
        method: 'POST', 
        body:  formData
    }).then((response)=>response.json())
    .then((response)=>{
        callback(response)
        // return response.msg;
    })
    .catch((error) => {
        console.error(error);
    });
}
