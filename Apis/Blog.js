import { serverApiUrl, serverBaseUrl } from "../Components/config";

export const fetchAllBlogs=(filter,callback)=>{
    let bannerData = new FormData();
    bannerData.append('fetchAllBlogs',true);
    bannerData.append('filter',filter);
    fetch(serverApiUrl+'blog.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}

export const fetchSingleBlog=(blogId,callback)=>{

    let bannerData = new FormData();
    bannerData.append('fetchSingleBlog',true);
    bannerData.append('blogId',blogId);
    fetch(serverApiUrl+'blog.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}