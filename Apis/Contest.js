import { serverApiUrl, serverBaseUrl } from "../Components/config";

export const fetchSingleContest=(contestId,callback) => {

    let bannerData = new FormData();
    bannerData.append('fetchSingleContest',true);
    bannerData.append('contestId',contestId);
    fetch(serverApiUrl+'contest.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}

export const checkContestant=(userId,contestId,callback) => {
    let bannerData = new FormData();
    bannerData.append('checkContestant',true);
    bannerData.append('userId',userId);
    bannerData.append('contestId',contestId);
    fetch(serverApiUrl+'contest.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}


export const fetchAllContests = (filter,callback) =>{
    let bannerData = new FormData();
    bannerData.append('fetchAllContest',true);
    bannerData.append('filter',filter);
    fetch(serverApiUrl+'contest.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: bannerData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}

//Api to fetch ongoing contests
export const fetchOngoingContests=(callback)=>{
    let videoData = new FormData();
    videoData.append('fetchOngoingContest',true);
    fetch(serverApiUrl+'contest.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: videoData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });

}


export const voteContestantForContest=(email,contestId,contestantId,callback)=>{

    let formData = new FormData();
    formData.append('voteParticipant',true);
    formData.append('email',email);
    formData.append('contestId',contestId);
    formData.append('contestantId',contestantId)
    fetch(serverApiUrl+'contest.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: formData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}

export const fetchContestWinner=(contestId,callback)=>{
    let formData = new FormData();
    formData.append('fetchWinner',true);
    formData.append('contestId',contestId);
    fetch(serverApiUrl+'contest.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: formData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}