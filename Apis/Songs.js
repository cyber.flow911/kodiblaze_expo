import { serverApiUrl, serverBaseUrl } from "../Components/config";

 //api to fetch all songs
 export const fetchAllSongs=(callback)=>{
    let songsData = new FormData();
    songsData.append('fetchSongs',true);
    fetch(serverApiUrl+'song.php', {
        method: 'POST',
        // headers: {
        // Accept: 'application/json',
        // 'Content-Type':'application/json' 
        // },  
        body: songsData
    })
    .then((response) => response.json())
    .then(result=>{  
        callback(result)
    })
    .catch((error) => {
        console.error(error);
    });
}


export const fetchSearchSongs=(userId,search,callback) => {

    var formData   = new FormData();
    formData.append("fetchUserSongsForVideo",true)
    formData.append("search",search)
    formData.append("userId" ,userId)
    fetch(serverApiUrl+'song.php',
    {
        method: 'POST', 
        body:  formData
    }).then((response)=>response.json())
    .then((response)=>{
        callback(response)
        // return response.msg;
    })
    .catch((error) => {
        console.error(error);
    });
}


export const checkSong=(songId,userId,callback) => {

    var formData   = new FormData();
    formData.append("checkUserSong",true)
    formData.append("songId",songId)
    formData.append("userId" ,userId)
    fetch(serverApiUrl+'song.php',
    {
        method: 'POST', 
        body:  formData
    }).then((response)=>response.json())
    .then((response)=>{
        callback(response)
        // return response.msg;
    })
    .catch((error) => {
        console.error(error);
    });
}
