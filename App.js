import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AppStartPoint from './Components/AppStartPoint';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import RootReducer from './Reducers'

let store = createStore(RootReducer)

export default function App() {
  return (
    <Provider store={store}> 
          <AppStartPoint />
    </Provider>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
